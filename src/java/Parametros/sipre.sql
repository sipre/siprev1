-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.5.24-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para sipre
DROP DATABASE IF EXISTS `sipre`;
CREATE DATABASE IF NOT EXISTS `sipre` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `sipre`;

-- Volcando estructura para tabla sipre.be_benficios
DROP TABLE IF EXISTS `be_benficios`;
CREATE TABLE IF NOT EXISTS `be_benficios` (
  `CodBeneficio` int(11) NOT NULL,
  `NombreBeneficio` varchar(50) COLLATE utf8_bin NOT NULL,
  `codTipoBeneficio` int(11) DEFAULT NULL,
  `valor` double(15,2) DEFAULT NULL,
  `desEmpleados` char(1) COLLATE utf8_bin DEFAULT NULL,
  `porEmpleado` decimal(3,2) DEFAULT NULL,
  `porEmpresa` decimal(3,2) DEFAULT NULL,
  `sobreSMLV` char(1) COLLATE utf8_bin DEFAULT NULL,
  `porSMLV` decimal(3,2) DEFAULT NULL,
  `conFijo` char(1) COLLATE utf8_bin DEFAULT NULL,
  `conIndefinido` char(1) COLLATE utf8_bin DEFAULT NULL,
  `conSena` char(1) COLLATE utf8_bin DEFAULT NULL,
  `beneficarios` char(1) COLLATE utf8_bin NOT NULL,
  `sueBasico` char(1) COLLATE utf8_bin DEFAULT NULL,
  `actUsuario` int(11) NOT NULL,
  `indActividad` varchar(1) COLLATE utf8_bin DEFAULT NULL,
  `actHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CodBeneficio`),
  KEY `fk_tipoBeneficio` (`codTipoBeneficio`),
  CONSTRAINT `fk_tipoBeneficio` FOREIGN KEY (`codTipoBeneficio`) REFERENCES `be_tipobeneficio` (`codTipoBeneficio`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.be_benficios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `be_benficios` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_benficios` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.be_solicitud
DROP TABLE IF EXISTS `be_solicitud`;
CREATE TABLE IF NOT EXISTS `be_solicitud` (
  `codSolicitud` int(11) NOT NULL AUTO_INCREMENT,
  `codtipoServicio` int(11) NOT NULL DEFAULT '0',
  `codTomador` int(11) NOT NULL DEFAULT '0',
  `estSolicitud` char(1) COLLATE utf8_bin NOT NULL DEFAULT 'N',
  `fecSolicitud` date NOT NULL,
  `observaciones` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'N''0',
  `valorEmpresa` double(15,0) DEFAULT NULL,
  `CodContratoSolicitante` int(11) DEFAULT NULL,
  `numfactura` varchar(25) COLLATE utf8_bin DEFAULT NULL,
  `valorEmpleado` double(15,0) DEFAULT NULL,
  `usuActividad` int(11) NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codSolicitud`),
  KEY `Be_Solicitud_FKcodser` (`codtipoServicio`),
  KEY `Be_Solicitud_FKTERCE` (`codTomador`),
  KEY `fk_solicitante` (`CodContratoSolicitante`),
  CONSTRAINT `FK_Be_solicitud_Bi_Terceros` FOREIGN KEY (`codTomador`) REFERENCES `bi_terceros` (`codTercero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_servisio` FOREIGN KEY (`codtipoServicio`) REFERENCES `be_tiposervicios` (`codTipoServicio`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitante` FOREIGN KEY (`CodContratoSolicitante`) REFERENCES `bi_contratosempleado` (`codContrato`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.be_solicitud: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `be_solicitud` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_solicitud` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.be_tipobeneficio
DROP TABLE IF EXISTS `be_tipobeneficio`;
CREATE TABLE IF NOT EXISTS `be_tipobeneficio` (
  `codTipoBeneficio` int(11) NOT NULL,
  `NombreBeneficio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Prescripcion` char(1) COLLATE utf8_bin DEFAULT NULL,
  `factura` char(1) COLLATE utf8_bin DEFAULT NULL,
  `presupuesto` char(1) COLLATE utf8_bin DEFAULT NULL,
  `usuActividad` int(11) DEFAULT NULL,
  `estActividad` char(1) COLLATE utf8_bin DEFAULT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codTipoBeneficio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.be_tipobeneficio: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `be_tipobeneficio` DISABLE KEYS */;
/*!40000 ALTER TABLE `be_tipobeneficio` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.be_tiposervicios
DROP TABLE IF EXISTS `be_tiposervicios`;
CREATE TABLE IF NOT EXISTS `be_tiposervicios` (
  `codTipoServicio` int(11) NOT NULL,
  `nomTipoServicio` varchar(20) COLLATE utf8_bin NOT NULL,
  `indActividad` char(1) COLLATE utf8_bin NOT NULL,
  `CodBeneficio` int(11) DEFAULT NULL,
  `CodContratoProveedor` int(11) DEFAULT NULL,
  `actUsuario` int(11) NOT NULL,
  `estActividad` char(1) COLLATE utf8_bin NOT NULL,
  `actHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`codTipoServicio`),
  UNIQUE KEY `Be_TipoServicios_be_tiposervicio$codTser_UNIQUE` (`codTipoServicio`),
  KEY `FK_BENEFICIO` (`CodBeneficio`),
  KEY `fk_prveedor` (`CodContratoProveedor`),
  CONSTRAINT `FK_BENEFICIO` FOREIGN KEY (`CodBeneficio`) REFERENCES `be_benficios` (`CodBeneficio`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_prveedor` FOREIGN KEY (`CodContratoProveedor`) REFERENCES `bi_contratoproveedor` (`CodContrato`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.be_tiposervicios: ~55 rows (aproximadamente)
/*!40000 ALTER TABLE `be_tiposervicios` DISABLE KEYS */;
INSERT INTO `be_tiposervicios` (`codTipoServicio`, `nomTipoServicio`, `indActividad`, `CodBeneficio`, `CodContratoProveedor`, `actUsuario`, `estActividad`, `actHora`) VALUES
	(1, 'EDUCACION', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(2, 'VIVIENDA', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(3, 'MEDICINA GENERAL', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(4, 'DEFUNCION', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(5, 'SERVICIO MEDICO', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(6, 'ANATOMIA PATOLOGICAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(7, 'ANESTESIOLOGIA PEDIA', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(8, 'ANESTESIOLOGIA Y REC', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(9, 'CARDIOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(10, 'CARDIOLOGIA PEDIATRI', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(11, 'CIRUGIA DE TORAX NO ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(12, 'CIRUGIA DE TORAX Y C', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(13, 'CIRUGIA GENERALÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(14, 'CIRUGIA ONCOLOGICAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(15, 'CIRUGIA PEDIATRICAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(16, 'CIRUGIA PLASTICA Y R', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(17, 'DERMATOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(18, 'ENDOCRINOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(19, 'GASTROENTEROLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(20, 'GERIATRIA Y GERONTOL', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(21, 'GINECOLOGIA ONCOLOGI', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(22, 'GINECOLOGIA Y OBSTET', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(23, 'HEMATOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(24, 'HEMATOLOGIA PEDIATRI', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(25, 'INFECTOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(26, 'INFECTOLOGIA PEDIATR', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(27, 'INMUNOLOGIA CLINICA ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(28, 'MEDICINA CRITICA PED', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(29, 'MEDICINA CRITICA Y T', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(30, 'MEDICINA DE EMERGENC', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(31, 'MEDICINA FAMILIAR Y ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(32, 'MEDICINA FISICA Y RE', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(33, 'MEDICINA INTERNAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(34, 'MEDICINA MATERNO FET', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(35, 'NEFROLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(36, 'NEFROLOGIA PEDIATRIC', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(37, 'NEONATOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(38, 'NEUMOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(39, 'NEUMOLOGIA PEDIATRIC', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(40, 'NEUROCIRUGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(41, 'NEUROLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(42, 'OFTALMOLOGIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(43, 'ONCOLOGIA MEDICAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(44, 'ORTOPEDIA Y TRAUMATO', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(45, 'ORTOPEDIA Y TRAUMATO', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(46, 'OTORRINOLARINGOLOGIA', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(47, 'PATOLOGIA PEDIATRICA', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(48, 'PEDIATRIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(49, 'PSICOLOGIA CLINICAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(50, 'PSIQUIATRIAÿ', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(51, 'RADIOLOGIA E IMAGENE', 'A', NULL, NULL, 0, '', '2016-08-04 15:42:52'),
	(52, '260-TRIAL- 52', '2', NULL, NULL, 0, '1', '2016-08-04 15:42:52'),
	(53, '126-TRIAL- 1', '3', NULL, NULL, 0, '1', '2016-08-04 15:42:52'),
	(54, '273-TRIAL- 192', '2', NULL, NULL, 0, '1', '2016-08-04 15:42:52'),
	(55, '90-TRIAL-UROLOGI 11', '1', NULL, NULL, 0, '2', '2016-08-04 15:42:52');
/*!40000 ALTER TABLE `be_tiposervicios` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.bi_contratoproveedor
DROP TABLE IF EXISTS `bi_contratoproveedor`;
CREATE TABLE IF NOT EXISTS `bi_contratoproveedor` (
  `CodContrato` int(11) NOT NULL,
  `codProveedor` int(11) DEFAULT NULL,
  `numContrato` int(11) DEFAULT NULL,
  `actUsuario` int(11) DEFAULT NULL,
  `estActividad` char(1) COLLATE utf8_bin DEFAULT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`CodContrato`),
  KEY `pk_tercero` (`codProveedor`),
  CONSTRAINT `pk_tercero` FOREIGN KEY (`codProveedor`) REFERENCES `bi_terceros` (`codTercero`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.bi_contratoproveedor: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bi_contratoproveedor` DISABLE KEYS */;
/*!40000 ALTER TABLE `bi_contratoproveedor` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.bi_contratosempleado
DROP TABLE IF EXISTS `bi_contratosempleado`;
CREATE TABLE IF NOT EXISTS `bi_contratosempleado` (
  `codContrato` int(11) NOT NULL,
  `codTercero` int(11) DEFAULT NULL,
  `TipContrato` char(1) COLLATE utf8_bin DEFAULT NULL,
  `fecInicio` date DEFAULT NULL,
  `fecFin` date DEFAULT NULL,
  `actUsuario` int(11) DEFAULT NULL,
  `estActividad` char(1) COLLATE utf8_bin DEFAULT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codContrato`),
  KEY `FK_TERCERO_CONTRATO` (`codTercero`),
  CONSTRAINT `FK_TERCERO_CONTRATO` FOREIGN KEY (`codTercero`) REFERENCES `bi_terceros` (`codTercero`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.bi_contratosempleado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bi_contratosempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `bi_contratosempleado` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.bi_sueldoempleado
DROP TABLE IF EXISTS `bi_sueldoempleado`;
CREATE TABLE IF NOT EXISTS `bi_sueldoempleado` (
  `codSueldo` int(11) NOT NULL,
  `codContrato` int(11) DEFAULT NULL,
  `Sueldo` double DEFAULT NULL,
  `actUsuario` int(11) DEFAULT NULL,
  `estActividad` char(1) COLLATE utf8_bin DEFAULT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codSueldo`),
  KEY `FK_CONTRATO_SUELDO` (`codContrato`),
  CONSTRAINT `FK_CONTRATO_SUELDO` FOREIGN KEY (`codContrato`) REFERENCES `bi_contratosempleado` (`codContrato`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.bi_sueldoempleado: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `bi_sueldoempleado` DISABLE KEYS */;
/*!40000 ALTER TABLE `bi_sueldoempleado` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.bi_terceros
DROP TABLE IF EXISTS `bi_terceros`;
CREATE TABLE IF NOT EXISTS `bi_terceros` (
  `codTercero` int(11) NOT NULL,
  `tipTercero` char(1) COLLATE utf8_bin NOT NULL,
  `tipIdentificacion` int(11) NOT NULL,
  `priNombre` varchar(20) COLLATE utf8_bin NOT NULL,
  `segNombre` varchar(20) COLLATE utf8_bin DEFAULT 'NULL',
  `priApellido` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `segApellido` varchar(20) COLLATE utf8_bin DEFAULT 'NULL',
  `munNacimiento` int(11) DEFAULT NULL,
  `fecNacimiento` date DEFAULT NULL,
  `indActividad` char(1) COLLATE utf8_bin NOT NULL,
  `dirResidencia` varchar(15) COLLATE utf8_bin NOT NULL,
  `telResidencia` int(11) NOT NULL,
  `corElectronico` varchar(45) COLLATE utf8_bin NOT NULL,
  `clave` char(25) COLLATE utf8_bin NOT NULL,
  `codMunicipioResidencia` int(11) NOT NULL,
  `codTipoBeneficiario` int(11) DEFAULT NULL,
  `codEmpleado` int(11) DEFAULT NULL,
  `actUsuario` varchar(15) COLLATE utf8_bin DEFAULT NULL,
  `actEsta` char(1) COLLATE utf8_bin NOT NULL,
  `actHora` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codTercero`),
  UNIQUE KEY `Bi_Terceros_bi_datosterceros$codTerc_UNIQUE` (`codTercero`),
  KEY `FK_Bi_Terceros_Gn_Municipios_naci` (`munNacimiento`),
  KEY `FK_Bi_Terceros_Gn_TipoIdentificaciones` (`tipIdentificacion`),
  KEY `fk_tipobeneficiario` (`codTipoBeneficiario`),
  KEY `FK_TERCERO_EMPLEADO` (`codEmpleado`),
  KEY `FK_Bi_Terceros_Gn_Municipios` (`codMunicipioResidencia`),
  CONSTRAINT `FK_Bi_Terceros_Gn_Municipios` FOREIGN KEY (`codMunicipioResidencia`) REFERENCES `gn_municipios` (`codMunicipio`),
  CONSTRAINT `FK_Bi_Terceros_Gn_Municipios_naci` FOREIGN KEY (`munNacimiento`) REFERENCES `gn_municipios` (`codMunicipio`),
  CONSTRAINT `FK_Bi_Terceros_Gn_TipoIdentificaciones` FOREIGN KEY (`tipIdentificacion`) REFERENCES `gn_tipoidentificaciones` (`codTipoIdentificacion`),
  CONSTRAINT `FK_TERCERO_EMPLEADO` FOREIGN KEY (`codEmpleado`) REFERENCES `bi_terceros` (`codTercero`),
  CONSTRAINT `fk_tipobeneficiario` FOREIGN KEY (`codTipoBeneficiario`) REFERENCES `gn_tipobeneficiarios` (`codTipoBeneficiarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.bi_terceros: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `bi_terceros` DISABLE KEYS */;
INSERT INTO `bi_terceros` (`codTercero`, `tipTercero`, `tipIdentificacion`, `priNombre`, `segNombre`, `priApellido`, `segApellido`, `munNacimiento`, `fecNacimiento`, `indActividad`, `dirResidencia`, `telResidencia`, `corElectronico`, `clave`, `codMunicipioResidencia`, `codTipoBeneficiario`, `codEmpleado`, `actUsuario`, `actEsta`, `actHora`) VALUES
	(1068180, 'A', 1, 'SIPRE', NULL, NULL, NULL, NULL, NULL, 'A', 'S', 1, 'A@A.COM', '1068180', 1, NULL, NULL, '1068180', 'A', '2017-02-26 00:15:26');
/*!40000 ALTER TABLE `bi_terceros` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_auditoria
DROP TABLE IF EXISTS `gn_auditoria`;
CREATE TABLE IF NOT EXISTS `gn_auditoria` (
  `codAuditoria` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipoModificacion` char(10) COLLATE utf8_bin DEFAULT NULL,
  `usuario` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tabla` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipTercero` char(1) COLLATE utf8_bin DEFAULT NULL,
  `codTercero` int(11) DEFAULT NULL,
  `TipIdentificacion` char(1) COLLATE utf8_bin DEFAULT NULL,
  `DesTercero` longtext COLLATE utf8_bin,
  `descripcion` longtext COLLATE utf8_bin,
  PRIMARY KEY (`codAuditoria`)
) ENGINE=InnoDB AUTO_INCREMENT=1898 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_auditoria: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_auditoria` DISABLE KEYS */;
INSERT INTO `gn_auditoria` (`codAuditoria`, `fecha`, `tipoModificacion`, `usuario`, `tabla`, `tipTercero`, `codTercero`, `TipIdentificacion`, `DesTercero`, `descripcion`) VALUES
	(1888, '2016-10-28 15:09:25', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, NULL, NULL, NULL),
	(1889, '2016-10-28 15:13:24', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', NULL, NULL),
	(1890, '2016-10-28 15:21:05', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL),
	(1891, '2016-10-28 15:22:36', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL),
	(1892, '2016-10-28 15:34:49', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL),
	(1893, '2016-10-28 15:38:15', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL),
	(1894, '2016-10-28 15:39:30', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL),
	(1895, '2016-10-28 15:40:52', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', 'indAct=A,codMuni=*,dirRes=CR19B 27 12 SUR'),
	(1896, '2016-10-28 15:54:20', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', 'indAct=A,codMuni=*,dirRes=CR19B 27 12 SUR'),
	(1897, '2016-10-28 15:58:21', 'I', 'sa', 'Bi_Terceros', 'E', 1030552386, '3', 'ALEJANDRO LOPEZ', NULL);
/*!40000 ALTER TABLE `gn_auditoria` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_departamentos
DROP TABLE IF EXISTS `gn_departamentos`;
CREATE TABLE IF NOT EXISTS `gn_departamentos` (
  `codDepartamento` int(11) NOT NULL,
  `codPais` int(11) NOT NULL,
  `nomDepartamento` varchar(30) COLLATE utf8_bin DEFAULT 'NULL',
  `usuActividad` char(15) COLLATE utf8_bin NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codDepartamento`),
  UNIQUE KEY `Gn_Departamentos_gn_departamento$codPais_UNIQUE` (`codDepartamento`),
  KEY `Gn_Departamentos_FKpais` (`codPais`),
  CONSTRAINT `gn_departamento$FKpais` FOREIGN KEY (`codPais`) REFERENCES `gn_pais` (`codPais`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_departamentos: ~29 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_departamentos` DISABLE KEYS */;
INSERT INTO `gn_departamentos` (`codDepartamento`, `codPais`, `nomDepartamento`, `usuActividad`, `tipActividad`, `horActividad`) VALUES
	(5, 169, 'ANTIOQUIA', '0', 'A', '2016-10-23 08:39:14'),
	(8, 169, 'ATLANTICO                     ', '0', 'A', '2016-10-23 08:39:28'),
	(11, 169, 'DISTRITO CAPITAL              ', '0', 'A', '2016-10-23 08:39:32'),
	(13, 169, 'BOLIVAR                       ', '0', 'A', '2016-10-23 08:39:36'),
	(15, 169, 'BOYACA                        ', '0', 'A', '2016-10-23 08:39:40'),
	(17, 169, 'CALDAS                        ', '0', 'A', '2016-10-23 08:39:44'),
	(18, 169, 'CAQUETA                       ', '0', 'A', '2016-10-23 08:39:47'),
	(19, 169, 'CAUCA                         ', '0', 'A', '2016-10-23 08:39:51'),
	(20, 169, 'CESAR                         ', '0', 'A', '2016-10-23 08:39:55'),
	(25, 169, 'CUNDINAMARCA                  ', '0', 'A', '2016-10-23 08:39:59'),
	(27, 169, 'CHOCO                         ', '0', 'A', '2016-10-23 08:40:02'),
	(30, 169, 'CORDOBA                       ', '0', 'A', '2016-10-23 08:40:06'),
	(41, 169, 'HUILA                         ', '0', 'A', '2016-10-23 08:40:10'),
	(44, 169, 'GUAJIRA                       ', '0', 'A', '2016-10-23 08:40:14'),
	(47, 169, 'MAGDALENA                     ', '0', 'A', '2016-10-23 08:40:18'),
	(50, 169, 'META                          ', '0', 'A', '2016-10-23 08:40:22'),
	(52, 169, 'NARI¥O                        ', '0', 'A', '2016-10-23 08:40:26'),
	(54, 169, 'NORTE DE SANTANDER            ', '0', 'A', '2016-10-23 08:40:29'),
	(63, 169, 'QUINDIO                       ', '0', 'A', '2016-10-23 08:40:33'),
	(66, 169, 'RISARALDA                     ', '0', 'A', '2016-10-23 08:40:37'),
	(68, 169, 'SANTANDER                     ', '0', 'A', '2016-10-23 08:40:41'),
	(70, 169, 'SUCRE                         ', '0', 'A', '2016-10-23 08:40:45'),
	(73, 169, 'TOLIMA                        ', '0', 'A', '2016-10-23 08:40:48'),
	(76, 169, 'VALLE DEL CAUCA               ', '0', 'A', '2016-10-23 08:40:52'),
	(81, 169, 'ARAUCA                        ', '0', 'A', '2016-10-23 08:40:56'),
	(85, 169, 'CASANARE                      ', '0', 'A', '2016-10-23 08:41:00'),
	(86, 169, 'PUTUMAYO                      ', '0', 'A', '2016-10-23 08:41:04'),
	(88, 169, 'SAN ANDRES                    ', '0', 'A', '2016-10-23 08:41:07'),
	(91, 169, 'AMAZONAS                      ', '0', 'A', '2016-10-23 08:41:11');
/*!40000 ALTER TABLE `gn_departamentos` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_docadjuntos
DROP TABLE IF EXISTS `gn_docadjuntos`;
CREATE TABLE IF NOT EXISTS `gn_docadjuntos` (
  `codDocumento` int(11) NOT NULL,
  `rutArchivo` int(11) NOT NULL,
  `tipDocumento` int(11) NOT NULL,
  `usuActividad` varchar(15) COLLATE utf8_bin NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codDocumento`),
  KEY `Gn_DocAdjuntos_fktipdocum_idx` (`codDocumento`),
  KEY `FK_Gn_DocAdjuntos_Gn_TipoDocumentos` (`tipDocumento`),
  CONSTRAINT `FK_Gn_DocAdjuntos_Gn_TipoDocumentos` FOREIGN KEY (`tipDocumento`) REFERENCES `gn_tipodocumentos` (`codTipoDocumentos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_docadjuntos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_docadjuntos` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_docadjuntos` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_docadjunto_bi_terceros
DROP TABLE IF EXISTS `gn_docadjunto_bi_terceros`;
CREATE TABLE IF NOT EXISTS `gn_docadjunto_bi_terceros` (
  `codTercero` int(11) NOT NULL,
  `CodDocumento` int(11) NOT NULL,
  PRIMARY KEY (`codTercero`,`CodDocumento`),
  KEY `FK_Bi_Terceros_Gn_DocAdjuntos` (`CodDocumento`),
  CONSTRAINT `FK_Bi_Terceros_Gn_DocAdjuntos` FOREIGN KEY (`CodDocumento`) REFERENCES `gn_docadjuntos` (`codDocumento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Gn_DocAdjunto_Bi_Terceros` FOREIGN KEY (`codTercero`) REFERENCES `bi_terceros` (`codTercero`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_docadjunto_bi_terceros: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_docadjunto_bi_terceros` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_docadjunto_bi_terceros` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_municipios
DROP TABLE IF EXISTS `gn_municipios`;
CREATE TABLE IF NOT EXISTS `gn_municipios` (
  `codMunicipio` int(11) NOT NULL AUTO_INCREMENT,
  `codDepartamento` int(11) NOT NULL,
  `codSuip` int(11) NOT NULL,
  `nomMunicipio` varchar(30) COLLATE utf8_bin DEFAULT 'NULL',
  `usuActividad` char(15) COLLATE utf8_bin NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codMunicipio`),
  UNIQUE KEY `Gn_Municipios_gn_municipio$codPais_UNIQUE` (`codMunicipio`),
  KEY `Gn_Municipios_FKmunicipio` (`codDepartamento`),
  CONSTRAINT `FK_Gn_Municipio_Gn_Departamento` FOREIGN KEY (`codDepartamento`) REFERENCES `gn_departamentos` (`codDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=454 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_municipios: ~475 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_municipios` DISABLE KEYS */;
INSERT INTO `gn_municipios` (`codMunicipio`, `codDepartamento`, `codSuip`, `nomMunicipio`, `usuActividad`, `tipActividad`, `horActividad`) VALUES
	(1, 5, 1, 'MEDELLIN', '0', 'A', '2016-08-02 00:00:00'),
	(2, 5, 2, 'ABEJORRAL', '0', 'A', '2016-08-02 00:00:00'),
	(3, 5, 3, 'BELLO', '0', 'A', '2016-08-02 00:00:00'),
	(4, 5, 4, 'ABRIAQUI', '0', 'A', '2016-08-02 00:00:00'),
	(5, 5, 21, 'ALEJANDRIA', '0', 'A', '2016-08-02 00:00:00'),
	(6, 5, 30, 'AMAGA', '0', 'A', '2016-08-02 00:00:00'),
	(7, 5, 31, 'AMALFI', '0', 'A', '2016-08-02 00:00:00'),
	(8, 5, 34, 'ANDES', '0', 'A', '2016-08-02 00:00:00'),
	(9, 5, 36, 'ANGELOPOLIS', '0', 'A', '2016-08-02 00:00:00'),
	(10, 5, 38, 'ANGOSTURA', '0', 'A', '2016-08-02 00:00:00'),
	(11, 5, 40, 'ANORI', '0', 'A', '2016-08-02 00:00:00'),
	(12, 5, 42, 'SANTAFE DE ANTIOQUIA', '0', 'A', '2016-08-02 00:00:00'),
	(13, 5, 44, 'ANZA', '0', 'A', '2016-08-02 00:00:00'),
	(14, 5, 45, 'APARTADO', '0', 'A', '2016-08-02 00:00:00'),
	(15, 5, 51, 'ARBOLETES', '0', 'A', '2016-08-02 00:00:00'),
	(16, 5, 55, 'ARGELIA', '0', 'A', '2016-08-02 00:00:00'),
	(17, 5, 59, 'ARMENIA', '0', 'A', '2016-08-02 00:00:00'),
	(18, 5, 79, 'BARBOSA', '0', 'A', '2016-08-02 00:00:00'),
	(19, 5, 91, 'BETANIA', '0', 'A', '2016-08-02 00:00:00'),
	(20, 5, 93, 'BETULIA', '0', 'A', '2016-08-02 00:00:00'),
	(21, 5, 101, 'CIUDAD BOLIVAR', '0', 'A', '2016-08-02 00:00:00'),
	(22, 5, 113, 'BURITICA', '0', 'A', '2016-08-02 00:00:00'),
	(23, 5, 129, 'CALDAS', '0', 'A', '2016-08-02 00:00:00'),
	(24, 5, 134, 'CAMPAMENTO', '0', 'A', '2016-08-02 00:00:00'),
	(25, 5, 138, 'CAÑASGORDAS', '0', 'A', '2016-08-02 00:00:00'),
	(26, 5, 142, 'CARACOLI', '0', 'A', '2016-08-02 00:00:00'),
	(27, 5, 145, 'CARAMANTA', '0', 'A', '2016-08-02 00:00:00'),
	(28, 5, 147, 'CAREPA', '0', 'A', '2016-08-02 00:00:00'),
	(29, 5, 154, 'CAUCASIA', '0', 'A', '2016-08-02 00:00:00'),
	(30, 5, 172, 'CHIGORODO                     ', '0', 'A', '2016-10-23 09:37:40'),
	(31, 5, 190, 'CISNEROS                      ', '0', 'A', '2016-10-23 09:37:44'),
	(32, 5, 197, 'COCORNA                       ', '0', 'A', '2016-10-23 09:37:48'),
	(33, 5, 206, 'CONCEPCION                    ', '0', 'A', '2016-10-23 09:37:52'),
	(34, 5, 209, 'CONCORDIA                     ', '0', 'A', '2016-10-23 09:37:55'),
	(35, 5, 212, 'COPACABANA                    ', '0', 'A', '2016-10-23 09:38:00'),
	(36, 5, 234, 'DABEIBA                       ', '0', 'A', '2016-10-23 09:38:04'),
	(37, 5, 237, 'DON MATIAS                    ', '0', 'A', '2016-10-23 09:38:08'),
	(38, 5, 240, 'EBEJICO                       ', '0', 'A', '2016-10-23 09:38:12'),
	(39, 5, 264, 'ENTRERRIOS                    ', '0', 'A', '2016-10-23 09:38:17'),
	(40, 5, 266, 'ENVIGADO                      ', '0', 'A', '2016-10-23 09:38:20'),
	(41, 5, 282, 'FREDONIA                      ', '0', 'A', '2016-10-23 09:38:25'),
	(42, 5, 306, 'GIRALDO                       ', '0', 'A', '2016-10-23 09:38:29'),
	(43, 5, 308, 'GIRARDOTA                     ', '0', 'A', '2016-10-23 09:38:32'),
	(44, 5, 310, 'GOMEZ PLATA                   ', '0', 'A', '2016-10-23 09:38:37'),
	(45, 5, 313, 'GRANADA                       ', '0', 'A', '2016-10-23 09:38:40'),
	(46, 5, 315, 'GUADALUPE                     ', '0', 'A', '2016-10-23 09:38:44'),
	(47, 5, 318, 'GUARNE                        ', '0', 'A', '2016-10-23 09:38:48'),
	(48, 5, 347, 'HELICONIA                     ', '0', 'A', '2016-10-23 09:38:51'),
	(49, 5, 353, 'HISPANIA                      ', '0', 'A', '2016-10-23 09:38:56'),
	(50, 5, 360, 'ITAGUI                        ', '0', 'A', '2016-10-23 09:39:00'),
	(51, 5, 361, 'ITUANGO                       ', '0', 'A', '2016-10-23 09:39:04'),
	(52, 5, 364, '41-TRIAL-JARDIA            167', '34-TRIAL-0  100', '2', '2016-10-23 09:39:07'),
	(53, 5, 368, '78-TRIAL-JERICO            258', '262-TRIAL-0 164', '5', '2016-10-23 09:39:11'),
	(54, 5, 376, '181-TRIAL-LA CEJA          27', '61-TRIAL-0  191', '2', '2016-10-23 09:39:16'),
	(55, 5, 380, '27-TRIAL-LA ESTRELLA       36', '291-TRIAL-0 204', '2', '2016-10-23 09:39:19'),
	(56, 5, 400, '292-TRIAL-LA UNION         82', '21-TRIAL-0  116', '2', '2016-10-23 09:39:23'),
	(57, 5, 411, '47-TRIAL-LIBORINA          126', '71-TRIAL-0  138', '6', '2016-10-23 09:39:27'),
	(58, 5, 425, '167-TRIAL-MACEO            199', '235-TRIAL-0 294', '2', '2016-10-23 09:39:31'),
	(59, 5, 467, '122-TRIAL-MONTEBELLO       33', '273-TRIAL-0 164', '1', '2016-10-23 09:39:34'),
	(60, 5, 480, '53-TRIAL-MUTATA            268', '47-TRIAL-0  44', '2', '2016-10-23 09:39:39'),
	(61, 5, 483, '237-TRIAL-NARI¥O           259', '23-TRIAL-0  141', '2', '2016-10-23 09:39:43'),
	(62, 5, 490, '16-TRIAL-NECOCLI           35', '290-TRIAL-0 42', '2', '2016-10-23 09:39:47'),
	(63, 5, 495, '40-TRIAL-NECHI             242', '64-TRIAL-0  148', '1', '2016-10-23 09:39:51'),
	(64, 5, 501, '290-TRIAL-OLAYA            129', '70-TRIAL-0  50', '6', '2016-10-23 09:39:54'),
	(65, 5, 541, '93-TRIAL-PE¥OL             248', '129-TRIAL-0 23', '8', '2016-10-23 09:39:58'),
	(66, 5, 543, '156-TRIAL-PEQUE            140', '166-TRIAL-0 176', '1', '2016-10-23 09:40:02'),
	(67, 5, 579, '144-TRIAL-PUERTO BERRIO    39', '26-TRIAL-0  223', '1', '2016-10-23 09:40:05'),
	(68, 5, 585, '218-TRIAL-PUERTO NARE      282', '129-TRIAL-0 41', '3', '2016-10-23 09:40:09'),
	(69, 5, 591, '139-TRIAL-PUERTO TRIUNFO   258', '204-TRIAL-0 30', '1', '2016-10-23 09:40:13'),
	(70, 5, 604, '173-TRIAL-REMEDIOS         186', '221-TRIAL-0 245', '2', '2016-10-23 09:40:17'),
	(71, 5, 607, '270-TRIAL-RETIRO           129', '77-TRIAL-0  273', '2', '2016-10-23 09:40:21'),
	(72, 5, 615, '286-TRIAL-RIONEGRO         90', '161-TRIAL-0 36', '1', '2016-10-23 09:40:25'),
	(73, 5, 631, '255-TRIAL-SABANETA         274', '131-TRIAL-0 52', '5', '2016-10-23 09:40:29'),
	(74, 5, 642, '141-TRIAL-SALGAR           124', '166-TRIAL-0 130', '2', '2016-10-23 09:40:33'),
	(75, 5, 647, '7-TRIAL-SAN ANDRES         237', '157-TRIAL-0 287', '1', '2016-10-23 09:40:37'),
	(76, 5, 649, '245-TRIAL-SAN CARLOS       209', '109-TRIAL-0 158', '2', '2016-10-23 09:40:41'),
	(77, 5, 652, '122-TRIAL-SAN FRANCISCO    46', '206-TRIAL-0 130', '2', '2016-10-23 09:40:44'),
	(78, 5, 656, '0-TRIAL-SAN JERONIMO       191', '162-TRIAL-0 155', '1', '2016-10-23 09:40:50'),
	(79, 5, 658, '24-TRIAL-SAN JOSE DE LA MO 137', '248-TRIAL-0 183', '2', '2016-10-23 09:40:53'),
	(80, 5, 659, '2-TRIAL-SAN JUAN DE URABA  50', '91-TRIAL-0  236', '7', '2016-10-23 09:40:57'),
	(81, 5, 660, '96-TRIAL-SAN LUIS          21', '48-TRIAL-0  99', '1', '2016-10-23 09:41:01'),
	(82, 5, 664, '181-TRIAL-SAN PEDRO        234', '53-TRIAL-0  199', '1', '2016-10-23 09:41:05'),
	(83, 5, 665, '0-TRIAL-SAN PEDRO DE URABA 188', '127-TRIAL-0 167', '1', '2016-10-23 09:41:08'),
	(84, 5, 667, '48-TRIAL-SAB RAFAEL        283', '107-TRIAL-0 21', '2', '2016-10-23 09:41:13'),
	(85, 5, 670, '13-TRIAL-SAN ROQUE         214', '209-TRIAL-0 116', '3', '2016-10-23 09:41:16'),
	(86, 5, 679, '200-TRIAL-SANTA BARBARA    149', '19-TRIAL-0  56', '2', '2016-10-23 09:41:20'),
	(87, 5, 686, '224-TRIAL-SATA ROSA DE OSO 208', '144-TRIAL-0 209', '2', '2016-10-23 09:41:24'),
	(88, 5, 690, '195-TRIAL-SANTO DOMINGO    85', '93-TRIAL-0  243', '2', '2016-10-23 09:41:27'),
	(89, 5, 697, '214-TRIAL-EL SANTUARIO     203', '248-TRIAL-0 0', '2', '2016-10-23 09:41:31'),
	(90, 5, 736, '180-TRIAL-SEGOVIA          296', '98-TRIAL-0  281', '8', '2016-10-23 09:41:35'),
	(91, 5, 756, '109-TRIAL-SONSON           157', '72-TRIAL-0  222', '2', '2016-10-23 09:41:39'),
	(92, 5, 761, '38-TRIAL-SOPETRAN          179', '190-TRIAL-0 257', '1', '2016-10-23 09:41:43'),
	(93, 5, 789, '15-TRIAL-TAMESIS           88', '256-TRIAL-0 111', '2', '2016-10-23 09:41:47'),
	(94, 5, 790, '272-TRIAL-TARAZA           255', '228-TRIAL-0 146', '2', '2016-10-23 09:41:50'),
	(95, 5, 792, '275-TRIAL-TARSO            233', '169-TRIAL-0 42', '1', '2016-10-23 09:41:55'),
	(96, 5, 809, '281-TRIAL-TITRIBI          198', '122-TRIAL-0 51', '1', '2016-10-23 09:41:59'),
	(97, 5, 819, '257-TRIAL-TOLEDO           276', '292-TRIAL-0 89', '2', '2016-10-23 09:42:03'),
	(98, 5, 837, '200-TRIAL-TURBO            110', '3-TRIAL-0   169', '1', '2016-10-23 09:42:07'),
	(99, 5, 842, '201-TRIAL-URAMITA          189', '255-TRIAL-0 223', '2', '2016-10-23 09:42:11'),
	(100, 5, 847, '182-TRIAL-URRAO            85', '88-TRIAL-0  226', '1', '2016-10-23 09:42:14'),
	(101, 5, 854, '232-TRIAL-VALDIVIA         32', '269-TRIAL-0 54', '2', '2016-10-23 09:42:18'),
	(102, 5, 856, '176-TRIAL-VALPARAISO       129', '268-TRIAL-0 192', '1', '2016-10-23 09:42:22'),
	(103, 5, 858, '134-TRIAL-VEGACHI          49', '241-TRIAL-0 212', '1', '2016-10-23 09:42:26'),
	(104, 5, 861, '118-TRIAL-VENECIA          153', '239-TRIAL-0 123', '7', '2016-10-23 09:42:29'),
	(105, 5, 873, '187-TRIAL-VIGA DEL FUERTE  229', '49-TRIAL-0  37', '6', '2016-10-23 09:42:33'),
	(106, 5, 885, '193-TRIAL-YALI             95', '297-TRIAL-0 16', '8', '2016-10-23 09:42:37'),
	(107, 5, 887, '188-TRIAL-YURUMAL          82', '155-TRIAL-0 234', '1', '2016-10-23 09:42:41'),
	(108, 5, 890, '116-TRIAL-YOLOMBO          271', '86-TRIAL-0  263', '1', '2016-10-23 09:42:45'),
	(109, 5, 895, '285-TRIAL-ZARAGOZA         253', '12-TRIAL-0  8', '3', '2016-10-23 09:42:49'),
	(110, 8, 1, '113-TRIAL-BARRANQUILLA     156', '121-TRIAL-0 58', '2', '2016-10-23 09:42:54'),
	(111, 8, 2, '181-TRIAL-SOLEDAD          244', '96-TRIAL-0  122', '2', '2016-10-23 09:42:58'),
	(112, 11, 1, '135-TRIAL-BOGOTA D.C.      50', '73-TRIAL-0  266', '4', '2016-10-23 09:43:01'),
	(113, 11, 20, '192-TRIAL-ENGATIVA         39', '153-TRIAL-0 224', '5', '2016-10-23 09:43:06'),
	(114, 11, 30, '245-TRIAL-FONTIBON         249', '286-TRIAL-0 213', '2', '2016-10-23 09:43:09'),
	(115, 11, 40, '68-TRIAL-SUBA              218', '187-TRIAL-0 5', '2', '2016-10-23 09:43:13'),
	(116, 11, 50, '2-TRIAL-USAQUEN            25', '77-TRIAL-0  214', '1', '2016-10-23 09:43:17'),
	(117, 11, 60, '234-TRIAL-USME             74', '72-TRIAL-0  59', '1', '2016-10-23 09:43:21'),
	(118, 11, 92, '287-TRIAL-BOSA             97', '18-TRIAL-0  77', '7', '2016-10-23 09:43:25'),
	(119, 13, 1, '263-TRIAL-CARTAGENA        268', '92-TRIAL-0  185', '1', '2016-10-23 09:43:29'),
	(120, 13, 6, '113-TRIAL-ACHI             127', '2-TRIAL-0   199', '2', '2016-10-23 09:43:32'),
	(121, 13, 30, '43-TRIAL-ALTOS DEL ROSARIO 124', '223-TRIAL-0 272', '1', '2016-10-23 09:43:36'),
	(122, 13, 42, '103-TRIAL-ARENAL           132', '105-TRIAL-0 293', '2', '2016-10-23 09:43:40'),
	(123, 13, 43, '92-TRIAL-SIMITI            142', '122-TRIAL-0 86', '1', '2016-10-23 09:43:44'),
	(124, 13, 52, '287-TRIAL-ARJONA           260', '213-TRIAL-0 74', '1', '2016-10-23 09:43:48'),
	(125, 13, 74, '235-TRIAL-BARRANCO DE LOBA 233', '211-TRIAL-0 260', '2', '2016-10-23 09:43:51'),
	(126, 13, 140, '85-TRIAL-CALAMAR           250', '140-TRIAL-0 194', '2', '2016-10-23 09:43:55'),
	(127, 13, 160, '119-TRIAL-CANTA GALLO      25', '176-TRIAL-0 94', '1', '2016-10-23 09:43:59'),
	(128, 13, 212, '271-TRIAL-CORDOBA          266', '178-TRIAL-0 93', '1', '2016-10-23 09:44:03'),
	(129, 13, 222, '118-TRIAL-CLEMENCIA        264', '119-TRIAL-0 52', '1', '2016-10-23 09:44:08'),
	(130, 13, 300, '160-TRIAL-HASTILLO DE LOBA 126', '10-TRIAL-0  257', '7', '2016-10-23 09:44:12'),
	(131, 13, 430, '276-TRIAL-MAGANGUE         227', '43-TRIAL-0  258', '2', '2016-10-23 09:44:16'),
	(132, 13, 433, '82-TRIAL-MAHATES           286', '165-TRIAL-0 187', '1', '2016-10-23 09:44:20'),
	(133, 13, 440, '225-TRIAL-MARGARITA        127', '229-TRIAL-0 128', '2', '2016-10-23 09:44:24'),
	(134, 13, 442, '2-TRIAL-MARIA LA BAJA      262', '123-TRIAL-0 296', '1', '2016-10-23 09:44:28'),
	(135, 13, 458, '295-TRIAL-MONTECRISTO      125', '64-TRIAL-0  160', '2', '2016-10-23 09:44:32'),
	(136, 13, 468, '230-TRIAL-MOMPOS           226', '211-TRIAL-0 171', '1', '2016-10-23 09:44:36'),
	(137, 13, 473, '153-TRIAL-MORALES          220', '90-TRIAL-0  224', '1', '2016-10-23 09:44:39'),
	(138, 13, 549, '140-TRIAL-PINILLOS         151', '62-TRIAL-0  29', '0', '2016-10-23 09:44:43'),
	(139, 13, 600, '58-TRIAL-RIO VIEJO         178', '265-TRIAL-0 107', '7', '2016-10-23 09:44:47'),
	(140, 13, 620, '258-TRIAL-SAN CRISTOBAL    139', '203-TRIAL-0 160', '1', '2016-10-23 09:44:51'),
	(141, 13, 647, '177-TRIAL-SAN ESTANISLAO   8', '113-TRIAL-0 187', '1', '2016-10-23 09:44:55'),
	(142, 13, 650, '60-TRIAL-SAN FERNANDO      228', '93-TRIAL-0  84', '2', '2016-10-23 09:44:59'),
	(143, 13, 654, '211-TRIAL-SAN JACINTO      204', '235-TRIAL-0 256', '7', '2016-10-23 09:45:04'),
	(144, 13, 657, '223-TRIAL-SAN JUAN NEPOMUC 85', '156-TRIAL-0 116', '1', '2016-10-23 09:45:07'),
	(145, 13, 667, '126-TRIAL-SAN MARTIN DE LO 157', '237-TRIAL-0 171', '1', '2016-10-23 09:45:11'),
	(146, 13, 670, '296-TRIAL-SAN PABLO        122', '217-TRIAL-0 212', '1', '2016-10-23 09:45:16'),
	(147, 13, 673, '185-TRIAL-SANTA CATALINA   41', '123-TRIAL-0 129', '2', '2016-10-23 09:45:20'),
	(148, 13, 744, '259-TRIAL-SIMITI           232', '96-TRIAL-0  155', '5', '2016-10-23 09:45:24'),
	(149, 13, 760, '284-TRIAL-SOPLAVIENTO      34', '54-TRIAL-0  172', '1', '2016-10-23 09:45:28'),
	(150, 13, 780, '32-TRIAL-TALAIGUA NUEVO    263', '207-TRIAL-0 83', '1', '2016-10-23 09:45:33'),
	(151, 13, 810, '167-TRIAL-TIQUISIO         48', '175-TRIAL-0 38', '1', '2016-10-23 09:45:36'),
	(152, 13, 836, '54-TRIAL-TURBACO           211', '241-TRIAL-0 75', '1', '2016-10-23 09:45:40'),
	(153, 13, 838, '221-TRIAL-TURBANA          170', '126-TRIAL-0 134', '2', '2016-10-23 09:45:44'),
	(154, 13, 873, '150-TRIAL-VILLANUEVA       298', '79-TRIAL-0  201', '1', '2016-10-23 09:45:48'),
	(155, 13, 894, '137-TRIAL-ZAMBRANO         134', '156-TRIAL-0 193', '2', '2016-10-23 09:45:53'),
	(156, 15, 1, '62-TRIAL-TUNJA             48', '281-TRIAL-0 0', '1', '2016-10-23 09:45:57'),
	(157, 15, 47, '55-TRIAL-AQUITANIA         255', '242-TRIAL-0 62', '1', '2016-10-23 09:46:01'),
	(158, 15, 51, '24-TRIAL-ARCABUCO          278', '252-TRIAL-0 143', '9', '2016-10-23 09:46:05'),
	(159, 15, 87, '140-TRIAL-BELEN            13', '275-TRIAL-0 272', '2', '2016-10-23 09:46:09'),
	(160, 15, 90, '117-TRIAL-BERBEO           232', '212-TRIAL-0 95', '2', '2016-10-23 09:46:13'),
	(161, 15, 92, '240-TRIAL-BETEITIVA        88', '185-TRIAL-0 190', '2', '2016-10-23 09:46:17'),
	(162, 15, 97, '190-TRIAL-BOAVITA          145', '153-TRIAL-0 114', '5', '2016-10-23 09:46:21'),
	(163, 15, 104, '144-TRIAL-BOYACA           158', '35-TRIAL-0  59', '9', '2016-10-23 09:46:26'),
	(164, 15, 109, '64-TRIAL-BUENAVISTA        181', '3-TRIAL-0   229', '7', '2016-10-23 09:46:30'),
	(165, 15, 114, '192-TRIAL-BUSBANZA         297', '149-TRIAL-0 156', '6', '2016-10-23 09:46:35'),
	(166, 15, 131, '167-TRIAL-CALDAS           141', '29-TRIAL-0  40', '2', '2016-10-23 09:46:39'),
	(167, 15, 135, '201-TRIAL-CAMPOHERMOSO     77', '115-TRIAL-0 283', '1', '2016-10-23 09:46:44'),
	(168, 15, 162, '24-TRIAL-CERINZA           201', '292-TRIAL-0 159', '2', '2016-10-23 09:46:48'),
	(169, 15, 172, '127-TRIAL-CHINAVITA        184', '175-TRIAL-0 186', '1', '2016-10-23 09:46:53'),
	(170, 15, 176, '287-TRIAL-CHIQUINQUIRA     147', '204-TRIAL-0 203', '2', '2016-10-23 09:46:57'),
	(171, 15, 180, '6-TRIAL-CHISCAS            263', '10-TRIAL-0  271', '1', '2016-10-23 09:47:01'),
	(172, 15, 183, '164-TRIAL-CHITA            42', '119-TRIAL-0 213', '9', '2016-10-23 09:47:05'),
	(173, 15, 185, '18-TRIAL-CHITARAQUE        232', '150-TRIAL-0 5', '1', '2016-10-23 09:47:09'),
	(174, 15, 187, '3-TRIAL-CHIVATA            22', '98-TRIAL-0  147', '8', '2016-10-23 09:47:13'),
	(175, 15, 189, '271-TRIAL-CIENEGA          164', '113-TRIAL-0 275', '2', '2016-10-23 09:47:17'),
	(176, 15, 204, '146-TRIAL-COMBITA          78', '269-TRIAL-0 262', '1', '2016-10-23 09:47:21'),
	(177, 15, 212, '89-TRIAL-COPER             44', '165-TRIAL-0 240', '1', '2016-10-23 09:47:25'),
	(178, 15, 215, '118-TRIAL-CORRALES         270', '1-TRIAL-0   123', '1', '2016-10-23 09:47:29'),
	(179, 15, 218, '152-TRIAL-COVARACHIA       187', '70-TRIAL-0  63', '2', '2016-10-23 09:47:33'),
	(180, 15, 223, '23-TRIAL-CUBARA            227', '200-TRIAL-0 269', '2', '2016-10-23 09:47:37'),
	(181, 15, 224, '28-TRIAL-CUCAITA           243', '147-TRIAL-0 288', '2', '2016-10-23 09:47:41'),
	(182, 15, 226, '209-TRIAL-CUITIVA          63', '249-TRIAL-0 181', '8', '2016-10-23 09:47:45'),
	(183, 15, 232, '8-TRIAL-CHIQUIZA           260', '221-TRIAL-0 258', '2', '2016-10-23 09:47:49'),
	(184, 15, 236, '46-TRIAL-CHIVOR            90', '149-TRIAL-0 243', '1', '2016-10-23 09:47:53'),
	(185, 15, 238, '148-TRIAL-DUITAMA          67', '36-TRIAL-0  83', '3', '2016-10-23 09:47:58'),
	(186, 15, 244, '185-TRIAL-EL COCUY         138', '253-TRIAL-0 129', '1', '2016-10-23 09:48:02'),
	(187, 15, 248, '123-TRIAL-EL ESPINO        59', '157-TRIAL-0 166', '1', '2016-10-23 09:48:06'),
	(188, 15, 272, '218-TRIAL-FIRAVITOBAI      26', '211-TRIAL-0 25', '2', '2016-10-23 09:48:10'),
	(189, 15, 276, '49-TRIAL-FLORESTA          196', '284-TRIAL-0 215', '2', '2016-10-23 09:48:15'),
	(190, 15, 293, '275-TRIAL-GACHANTIVA       213', '242-TRIAL-0 296', '4', '2016-10-23 09:48:20'),
	(191, 15, 296, '26-TRIAL-GAMEZA            206', '73-TRIAL-0  129', '4', '2016-10-23 09:48:24'),
	(192, 15, 299, '226-TRIAL-GARAGOA          112', '175-TRIAL-0 93', '6', '2016-10-23 09:48:28'),
	(193, 15, 317, '36-TRIAL-GUACAMAYAS        41', '214-TRIAL-0 294', '1', '2016-10-23 09:48:32'),
	(194, 15, 322, '236-TRIAL-GUATEQUE         238', '82-TRIAL-0  155', '1', '2016-10-23 09:48:36'),
	(195, 15, 325, '230-TRIAL-GUAYATA          141', '225-TRIAL-0 211', '2', '2016-10-23 09:48:40'),
	(196, 15, 332, '190-TRIAL-GUICAN           150', '262-TRIAL-0 34', '9', '2016-10-23 09:48:44'),
	(197, 15, 362, '116-TRIAL-IZA              252', '208-TRIAL-0 62', '3', '2016-10-23 09:48:48'),
	(198, 15, 367, '103-TRIAL-JENESANO         134', '203-TRIAL-0 156', '1', '2016-10-23 09:48:53'),
	(199, 15, 368, '17-TRIAL-JERICO            13', '109-TRIAL-0 28', '1', '2016-10-23 09:48:57'),
	(200, 15, 377, '18-TRIAL-LABRANZAGRANDE    58', '50-TRIAL-0  155', '1', '2016-10-23 09:49:01'),
	(201, 15, 380, '203-TRIAL-LA CAPILLA       76', '243-TRIAL-0 209', '2', '2016-10-23 09:49:06'),
	(202, 15, 401, '289-TRIAL-LA VICTORIA      48', '82-TRIAL-0  153', '7', '2016-10-23 09:49:10'),
	(203, 15, 403, '2-TRIAL-LA UVITA           23', '231-TRIAL-0 169', '2', '2016-10-23 09:49:15'),
	(204, 15, 407, '108-TRIAL-VILLA DE LEYVA   119', '271-TRIAL-0 3', '4', '2016-10-23 09:49:19'),
	(205, 15, 425, '104-TRIAL-MACANAL          92', '285-TRIAL-0 113', '9', '2016-10-23 09:49:23'),
	(206, 15, 442, '122-TRIAL-MARIPI           238', '137-TRIAL-0 110', '2', '2016-10-23 09:49:26'),
	(207, 15, 455, '208-TRIAL-MIRAFLORES       61', '59-TRIAL-0  193', '1', '2016-10-23 09:49:30'),
	(208, 15, 464, '37-TRIAL-MONGUA            69', '58-TRIAL-0  0', '1', '2016-10-23 09:49:34'),
	(209, 15, 466, '117-TRIAL-MONGUI           15', '255-TRIAL-0 15', '3', '2016-10-23 09:49:38'),
	(210, 15, 469, '212-TRIAL-MONIQUIRA        188', '182-TRIAL-0 154', '1', '2016-10-23 09:49:42'),
	(211, 15, 476, '184-TRIAL-MOTAVITA         174', '280-TRIAL-0 115', '1', '2016-10-23 09:49:46'),
	(212, 15, 480, '115-TRIAL-MUZO             179', '10-TRIAL-0  98', '2', '2016-10-23 09:49:50'),
	(213, 15, 491, '277-TRIAL-NOBSA            132', '256-TRIAL-0 189', '1', '2016-10-23 09:49:53'),
	(214, 15, 494, '41-TRIAL-NUEVO COLON       190', '223-TRIAL-0 63', '2', '2016-10-23 09:49:57'),
	(215, 15, 500, '178-TRIAL-OICATA           0', '271-TRIAL-0 85', '7', '2016-10-23 09:50:01'),
	(216, 15, 507, '233-TRIAL-OTANCHE          67', '53-TRIAL-0  195', '6', '2016-10-23 09:50:05'),
	(217, 15, 511, '76-TRIAL-PACHAVITA         29', '150-TRIAL-0 198', '9', '2016-10-23 09:50:09'),
	(218, 15, 514, '186-TRIAL-PAEZ             80', '216-TRIAL-0 249', '2', '2016-10-23 09:50:13'),
	(219, 15, 516, '279-TRIAL-PAIPA            64', '21-TRIAL-0  5', '1', '2016-10-23 09:50:17'),
	(220, 15, 518, '16-TRIAL-PAJARITO          126', '166-TRIAL-0 287', '8', '2016-10-23 09:50:20'),
	(221, 15, 531, '140-TRIAL-PAUNA            286', '21-TRIAL-0  262', '2', '2016-10-23 09:50:24'),
	(222, 15, 533, '209-TRIAL-PAYA             15', '202-TRIAL-0 173', '2', '2016-10-23 09:50:28'),
	(223, 15, 537, '45-TRIAL-PAZ DE RIO        262', '223-TRIAL-0 231', '6', '2016-10-23 09:50:31'),
	(224, 15, 542, '18-TRIAL-PESCA             202', '107-TRIAL-0 7', '8', '2016-10-23 09:50:35'),
	(225, 15, 550, '136-TRIAL-PISBA            230', '114-TRIAL-0 109', '1', '2016-10-23 09:50:39'),
	(226, 15, 572, '290-TRIAL-PUERTO BOYACA    293', '296-TRIAL-0 52', '1', '2016-10-23 09:50:44'),
	(227, 15, 580, '8-TRIAL-QUIPAMA            248', '291-TRIAL-0 212', '3', '2016-10-23 09:50:47'),
	(228, 15, 599, '239-TRIAL-RAMIRIQUI        158', '122-TRIAL-0 4', '9', '2016-10-23 09:50:51'),
	(229, 15, 600, '69-TRIAL-RAQUIRA           279', '238-TRIAL-0 23', '1', '2016-10-23 09:50:55'),
	(230, 15, 621, '259-TRIAL-RONDON           98', '286-TRIAL-0 196', '2', '2016-10-23 09:50:59'),
	(231, 15, 632, '158-TRIAL-SABOYA           22', '246-TRIAL-0 92', '1', '2016-10-23 09:51:03'),
	(232, 15, 638, '147-TRIAL-SACHICA          58', '2-TRIAL-0   207', '1', '2016-10-23 09:51:07'),
	(233, 15, 646, '192-TRIAL-SAMACA           200', '78-TRIAL-0  99', '5', '2016-10-23 09:51:11'),
	(234, 15, 660, '82-TRIAL-SAN EDUARDO       240', '115-TRIAL-0 75', '6', '2016-10-23 09:51:16'),
	(235, 15, 664, '136-TRIAL-SAN JOSE DE PARE 97', '218-TRIAL-0 97', '2', '2016-10-23 09:51:20'),
	(236, 15, 667, '116-TRIAL-SAN LUIS DE GACE 230', '249-TRIAL-0 125', '5', '2016-10-23 09:51:24'),
	(237, 15, 673, '220-TRIAL-SAN MATEO        140', '160-TRIAL-0 147', '2', '2016-10-23 09:51:28'),
	(238, 15, 676, '275-TRIAL-SAN MIGUEL DE SE 192', '161-TRIAL-0 254', '1', '2016-10-23 09:51:31'),
	(239, 15, 681, '14-TRIAL-SAN PABLO DE BORB 246', '188-TRIAL-0 69', '2', '2016-10-23 09:51:35'),
	(240, 15, 686, '75-TRIAL-SANTANA           215', '121-TRIAL-0 175', '1', '2016-10-23 09:51:39'),
	(241, 15, 690, '34-TRIAL-SANTA MARIA       270', '5-TRIAL-0   164', '5', '2016-10-23 09:51:43'),
	(242, 15, 693, '61-TRIAL-SANTA ROSA DE VIT 224', '249-TRIAL-0 69', '3', '2016-10-23 09:51:47'),
	(243, 15, 696, '150-TRIAL-SANTA SOFIA      233', '125-TRIAL-0 110', '2', '2016-10-23 09:51:50'),
	(244, 15, 720, '37-TRIAL-SATIVANORTE       78', '93-TRIAL-0  136', '1', '2016-10-23 09:51:54'),
	(245, 15, 723, '91-TRIAL-SATIVASUR         149', '235-TRIAL-0 105', '1', '2016-10-23 09:51:58'),
	(246, 15, 740, '137-TRIAL-SIACHOQUE        223', '164-TRIAL-0 70', '1', '2016-10-23 09:52:01'),
	(247, 15, 753, '81-TRIAL-SOATA             185', '152-TRIAL-0 73', '1', '2016-10-23 09:52:06'),
	(248, 15, 755, '276-TRIAL-SOCOTA           126', '196-TRIAL-0 72', '2', '2016-10-23 09:52:09'),
	(249, 15, 757, '74-TRIAL-SOCHA             219', '43-TRIAL-0  211', '1', '2016-10-23 09:52:14'),
	(250, 15, 759, '19-TRIAL-SOGAMOSO          165', '205-TRIAL-0 85', '1', '2016-10-23 09:52:18'),
	(251, 15, 761, '115-TRIAL-SOMONDOCO        209', '264-TRIAL-0 166', '9', '2016-10-23 09:52:22'),
	(252, 15, 762, '9-TRIAL-SORA               0', '195-TRIAL-0 273', '2', '2016-10-23 09:52:25'),
	(253, 15, 763, '272-TRIAL-SOTAQUIRA        268', '58-TRIAL-0  231', '1', '2016-10-23 09:52:29'),
	(254, 15, 764, '10-TRIAL-SORACA            222', '174-TRIAL-0 79', '1', '2016-10-23 09:52:33'),
	(255, 15, 774, '282-TRIAL-SUSACON          191', '195-TRIAL-0 64', '2', '2016-10-23 09:52:37'),
	(256, 15, 776, '202-TRIAL-SUTAMARCHAN      55', '260-TRIAL-0 174', '7', '2016-10-23 09:52:42'),
	(257, 15, 778, '22-TRIAL-SUTATENZA         47', '277-TRIAL-0 189', '1', '2016-10-23 09:52:46'),
	(258, 15, 790, '294-TRIAL-TASCO            150', '143-TRIAL-0 154', '8', '2016-10-23 09:52:50'),
	(259, 15, 798, '272-TRIAL-TENZA            39', '228-TRIAL-0 212', '1', '2016-10-23 09:52:53'),
	(260, 15, 804, '108-TRIAL-TIBANA           215', '108-TRIAL-0 123', '5', '2016-10-23 09:52:57'),
	(261, 15, 806, '104-TRIAL-TIBASOSA         86', '119-TRIAL-0 58', '2', '2016-10-23 09:53:01'),
	(262, 15, 808, '66-TRIAL-TINJACA           100', '267-TRIAL-0 292', '1', '2016-10-23 09:53:04'),
	(263, 15, 810, '256-TRIAL-TIPACOQUE        74', '147-TRIAL-0 221', '2', '2016-10-23 09:53:08'),
	(264, 15, 814, '231-TRIAL-TOCA             276', '83-TRIAL-0  248', '1', '2016-10-23 09:53:12'),
	(265, 15, 822, '118-TRIAL-TOTA             76', '220-TRIAL-0 211', '8', '2016-10-23 09:53:16'),
	(266, 15, 832, '290-TRIAL-TUNUNGUA         225', '124-TRIAL-0 186', '1', '2016-10-23 09:53:20'),
	(267, 15, 835, '243-TRIAL-TURMEQUE         34', '77-TRIAL-0  168', '1', '2016-10-23 09:53:23'),
	(268, 15, 837, '96-TRIAL-TUTA              283', '28-TRIAL-0  227', '2', '2016-10-23 09:53:27'),
	(269, 15, 839, '197-TRIAL-TUTAZA           212', '103-TRIAL-0 227', '8', '2016-10-23 09:53:31'),
	(270, 15, 842, '208-TRIAL-UBITA            285', '238-TRIAL-0 237', '4', '2016-10-23 09:53:35'),
	(271, 15, 861, '1-TRIAL-VENTAQUEMADA       150', '228-TRIAL-0 11', '2', '2016-10-23 09:53:39'),
	(272, 15, 879, '92-TRIAL-VIRACACHA         254', '269-TRIAL-0 81', '1', '2016-10-23 09:53:43'),
	(273, 15, 897, '13-TRIAL-ZETAQUIRA         293', '34-TRIAL-0  272', '2', '2016-10-23 09:53:48'),
	(274, 17, 1, '1-TRIAL-MANIZALES          242', '77-TRIAL-0  77', '2', '2016-10-23 09:53:51'),
	(275, 17, 2, '264-TRIAL-SUPIA            181', '90-TRIAL-0  123', '1', '2016-10-23 09:53:55'),
	(276, 17, 3, '79-TRIAL-AGUADAS           95', '69-TRIAL-0  227', '4', '2016-10-23 09:53:59'),
	(277, 18, 1, '282-TRIAL-FLORENCIA        258', '126-TRIAL-0 187', '1', '2016-10-23 09:54:03'),
	(278, 19, 1, '251-TRIAL-POPAYAN          158', '13-TRIAL-0  260', '2', '2016-10-23 09:54:07'),
	(279, 20, 1, '42-TRIAL-VALLEDEUPAR       210', '272-TRIAL-0 228', '1', '2016-10-23 09:54:11'),
	(280, 20, 11, '18-TRIAL-AGUACHICA         203', '167-TRIAL-0 265', '1', '2016-10-23 09:54:15'),
	(281, 25, 1, '257-TRIAL-AGUA DE DIOS     250', '114-TRIAL-0 298', '2', '2016-10-23 09:54:19'),
	(282, 25, 19, '263-TRIAL-ALBAN            56', '107-TRIAL-0 178', '2', '2016-10-23 09:54:23'),
	(283, 25, 35, '65-TRIAL-ANAPOIMA          275', '86-TRIAL-0  186', '3', '2016-10-23 09:54:27'),
	(284, 25, 40, '130-TRIAL-ANOLAIMA         248', '228-TRIAL-0 92', '1', '2016-10-23 09:54:30'),
	(285, 25, 86, '166-TRIAL-BELTRAN          235', '10-TRIAL-0  199', '1', '2016-10-23 09:54:34'),
	(286, 25, 95, '182-TRIAL-BITUIMA          128', '252-TRIAL-0 269', '4', '2016-10-23 09:54:38'),
	(287, 25, 99, '8-TRIAL-BOJACA             252', '247-TRIAL-0 32', '2', '2016-10-23 09:54:42'),
	(288, 25, 120, '264-TRIAL-CABRERA          197', '143-TRIAL-0 49', '1', '2016-10-23 09:54:46'),
	(289, 25, 123, '189-TRIAL-CACHIPAY         200', '12-TRIAL-0  48', '2', '2016-10-23 09:54:49'),
	(290, 25, 126, '174-TRIAL-CAJICA           133', '191-TRIAL-0 200', '5', '2016-10-23 09:54:53'),
	(291, 25, 148, '197-TRIAL-CAPARRAPI        119', '80-TRIAL-0  78', '3', '2016-10-23 09:54:58'),
	(292, 25, 151, '40-TRIAL-CAQUEZA           287', '299-TRIAL-0 25', '8', '2016-10-23 09:55:02'),
	(293, 25, 154, '292-TRIAL-CARMEN DE CARUPA 193', '52-TRIAL-0  111', '6', '2016-10-23 09:55:06'),
	(294, 25, 168, '40-TRIAL-CHAGUANI          297', '85-TRIAL-0  229', '4', '2016-10-23 09:55:11'),
	(295, 25, 175, '291-TRIAL-CHIA             192', '10-TRIAL-0  49', '2', '2016-10-23 09:55:15'),
	(296, 25, 178, '71-TRIAL-CHIPAQUE          277', '73-TRIAL-0  93', '1', '2016-10-23 09:55:19'),
	(297, 25, 181, '26-TRIAL-CHOACHI           76', '290-TRIAL-0 282', '7', '2016-10-23 09:55:23'),
	(298, 25, 183, '18-TRIAL-CHOCONTA          89', '159-TRIAL-0 149', '2', '2016-10-23 09:55:26'),
	(299, 25, 200, '180-TRIAL-COGUA            8', '67-TRIAL-0  8', '1', '2016-10-23 09:55:31'),
	(300, 25, 214, '70-TRIAL-COTA              207', '96-TRIAL-0  74', '2', '2016-10-23 09:55:34'),
	(301, 25, 245, '119-TRIAL-EL COLEGIO       261', '256-TRIAL-0 290', '2', '2016-10-23 09:55:38'),
	(302, 25, 258, '216-TRIAL-EL PE¥ON         132', '252-TRIAL-0 41', '1', '2016-10-23 09:55:42'),
	(303, 25, 269, '162-TRIAL-FACATATIVA       196', '160-TRIAL-0 215', '1', '2016-10-23 09:55:45'),
	(304, 25, 279, '36-TRIAL-FOMEQUE           180', '198-TRIAL-0 32', '2', '2016-10-23 09:55:50'),
	(305, 25, 281, '40-TRIAL-FOSCA             217', '206-TRIAL-0 270', '2', '2016-10-23 09:55:53'),
	(306, 25, 290, '49-TRIAL-FUSAGASUGA        223', '258-TRIAL-0 105', '2', '2016-10-23 09:55:57'),
	(307, 25, 295, '96-TRIAL-GACHANCIPA        16', '78-TRIAL-0  278', '7', '2016-10-23 09:56:01'),
	(308, 25, 297, '277-TRIAL-GACHETA          150', '207-TRIAL-0 29', '8', '2016-10-23 09:56:05'),
	(309, 25, 299, '278-TRIAL-GAMA             76', '153-TRIAL-0 199', '8', '2016-10-23 09:56:09'),
	(310, 25, 307, '93-TRIAL-GIRARDOT          108', '172-TRIAL-0 143', '2', '2016-10-23 09:56:12'),
	(311, 25, 312, '268-TRIAL-GRANADA          255', '91-TRIAL-0  273', '2', '2016-10-23 09:56:16'),
	(312, 25, 317, '251-TRIAL-GUACHETA         186', '44-TRIAL-0  246', '7', '2016-10-23 09:56:20'),
	(313, 25, 320, '229-TRIAL-GUADUAS          216', '174-TRIAL-0 191', '1', '2016-10-23 09:56:24'),
	(314, 25, 322, '46-TRIAL-GUASCA            93', '91-TRIAL-0  215', '2', '2016-10-23 09:56:28'),
	(315, 25, 324, '240-TRIAL-GUATAQUI         252', '236-TRIAL-0 151', '1', '2016-10-23 09:56:33'),
	(316, 25, 326, '262-TRIAL-GUATAVITA        155', '83-TRIAL-0  294', '1', '2016-10-23 09:56:37'),
	(317, 25, 335, '65-TRIAL-GUAYABETAL        65', '113-TRIAL-0 261', '2', '2016-10-23 09:56:41'),
	(318, 25, 339, '78-TRIAL-GUTIERREZ         40', '111-TRIAL-0 147', '4', '2016-10-23 09:56:45'),
	(319, 25, 368, '275-TRIAL-JERUSALEN        289', '150-TRIAL-0 149', '3', '2016-10-23 09:56:49'),
	(320, 25, 372, '14-TRIAL-JUNIN             182', '7-TRIAL-0   132', '1', '2016-10-23 09:56:52'),
	(321, 25, 377, '22-TRIAL-LA CALERA         82', '10-TRIAL-0  241', '3', '2016-10-23 09:56:57'),
	(322, 25, 386, '105-TRIAL-LA MESA          179', '21-TRIAL-0  238', '1', '2016-10-23 09:57:01'),
	(323, 25, 394, '208-TRIAL-LA PALMA         46', '76-TRIAL-0  259', '1', '2016-10-23 09:57:05'),
	(324, 25, 398, '66-TRIAL-LA PE¥A           86', '155-TRIAL-0 228', '2', '2016-10-23 09:57:08'),
	(325, 25, 402, '153-TRIAL-LA VEGA          77', '148-TRIAL-0 203', '6', '2016-10-23 09:57:12'),
	(326, 25, 407, '282-TRIAL-LENGUAZAQUE      155', '97-TRIAL-0  206', '5', '2016-10-23 09:57:16'),
	(327, 25, 408, '196-TRIAL-GACHALA          181', '221-TRIAL-0 155', '4', '2016-10-23 09:57:20'),
	(328, 25, 426, '18-TRIAL-MACHETA           135', '276-TRIAL-0 274', '1', '2016-10-23 09:57:24'),
	(329, 25, 430, '74-TRIAL-MADRID            253', '22-TRIAL-0  135', '1', '2016-10-23 09:57:29'),
	(330, 25, 436, '53-TRIAL-MANTA             32', '247-TRIAL-0 180', '1', '2016-10-23 09:57:33'),
	(331, 25, 438, '150-TRIAL-MEDINA           101', '61-TRIAL-0  99', '1', '2016-10-23 09:57:36'),
	(332, 25, 473, '16-TRIAL-MOSQUERA          73', '61-TRIAL-0  145', '1', '2016-10-23 09:57:40'),
	(333, 25, 483, '50-TRIAL-NARI¥O            53', '281-TRIAL-0 87', '2', '2016-10-23 09:57:44'),
	(334, 25, 486, '43-TRIAL-NEMOCON           65', '272-TRIAL-0 129', '8', '2016-10-23 09:57:48'),
	(335, 25, 488, '276-TRIAL-NILO             181', '147-TRIAL-0 190', '1', '2016-10-23 09:57:52'),
	(336, 25, 489, '272-TRIAL-NIMAIMA          32', '89-TRIAL-0  20', '6', '2016-10-23 09:57:56'),
	(337, 25, 491, '58-TRIAL-NOCAIMA           193', '106-TRIAL-0 178', '1', '2016-10-23 09:58:01'),
	(338, 25, 506, '171-TRIAL-VENECIA          166', '96-TRIAL-0  197', '1', '2016-10-23 09:58:05'),
	(339, 25, 513, '229-TRIAL-PACHO            88', '109-TRIAL-0 284', '2', '2016-10-23 09:58:09'),
	(340, 25, 518, '17-TRIAL-PAIME             115', '126-TRIAL-0 84', '1', '2016-10-23 09:58:12'),
	(341, 25, 530, '128-TRIAL-PARATEBUENO      97', '218-TRIAL-0 90', '1', '2016-10-23 09:58:16'),
	(342, 25, 535, '86-TRIAL-PASCA             299', '120-TRIAL-0 10', '2', '2016-10-23 09:58:20'),
	(343, 25, 572, '115-TRIAL-PUERTO SALGAR    85', '18-TRIAL-0  280', '1', '2016-10-23 09:58:24'),
	(344, 25, 580, '287-TRIAL-PULI             244', '86-TRIAL-0  107', '1', '2016-10-23 09:58:27'),
	(345, 25, 592, '174-TRIAL-QUEBRADANEGRA    31', '252-TRIAL-0 271', '6', '2016-10-23 09:58:31'),
	(346, 25, 594, '85-TRIAL-QUETAME           37', '111-TRIAL-0 204', '7', '2016-10-23 09:58:36'),
	(347, 25, 596, '268-TRIAL-QUIPILE          222', '213-TRIAL-0 200', '2', '2016-10-23 09:58:39'),
	(348, 25, 599, '38-TRIAL-APULO             88', '155-TRIAL-0 89', '1', '2016-10-23 09:58:43'),
	(349, 25, 612, '193-TRIAL-RICAURTE         84', '187-TRIAL-0 261', '9', '2016-10-23 09:58:47'),
	(350, 25, 645, '201-TRIAL-SAN ANTONIO DE T 82', '47-TRIAL-0  65', '2', '2016-10-23 09:58:51'),
	(351, 25, 649, '84-TRIAL-SAN BERNARDO      195', '25-TRIAL-0  221', '6', '2016-10-23 09:58:55'),
	(352, 25, 653, '72-TRIAL-SAN CAYETANO      6', '56-TRIAL-0  43', '9', '2016-10-23 09:58:59'),
	(353, 25, 658, '180-TRIAL-SAN FRANCISCO    168', '111-TRIAL-0 213', '2', '2016-10-23 09:59:03'),
	(354, 25, 662, '216-TRIAL-SAN JUAN DE RIO  79', '268-TRIAL-0 240', '3', '2016-10-23 09:59:07'),
	(355, 25, 718, '79-TRIAL-SASAIMA           263', '259-TRIAL-0 253', '3', '2016-10-23 09:59:10'),
	(356, 25, 736, '65-TRIAL-SESQUILE          174', '220-TRIAL-0 135', '1', '2016-10-23 09:59:14'),
	(357, 25, 740, '155-TRIAL-SIBATE           25', '171-TRIAL-0 208', '5', '2016-10-23 09:59:19'),
	(358, 25, 743, '202-TRIAL-SILVANIA         132', '105-TRIAL-0 240', '1', '2016-10-23 09:59:23'),
	(359, 25, 745, '85-TRIAL-SIMIJACA          62', '80-TRIAL-0  36', '2', '2016-10-23 09:59:27'),
	(360, 25, 754, '8-TRIAL-SOACHA             180', '240-TRIAL-0 76', '5', '2016-10-23 09:59:31'),
	(361, 25, 758, '240-TRIAL-SOPO             146', '74-TRIAL-0  173', '1', '2016-10-23 09:59:35'),
	(362, 25, 769, '235-TRIAL-SUBACHOQUE       272', '0-TRIAL-0   107', '1', '2016-10-23 09:59:39'),
	(363, 25, 772, '241-TRIAL-SUESCA           188', '181-TRIAL-0 68', '1', '2016-10-23 09:59:42'),
	(364, 25, 777, '25-TRIAL-SUPATA            109', '112-TRIAL-0 136', '5', '2016-10-23 09:59:47'),
	(365, 25, 779, '143-TRIAL-SUSA             142', '21-TRIAL-0  222', '2', '2016-10-23 09:59:50'),
	(366, 25, 780, '218-TRIAL-FUNZA            68', '17-TRIAL-0  114', '2', '2016-10-23 09:59:54'),
	(367, 25, 781, '35-TRIAL-SUTATAUSA         159', '169-TRIAL-0 295', '2', '2016-10-23 09:59:58'),
	(368, 25, 785, '79-TRIAL-TABIO             199', '5-TRIAL-0   191', '6', '2016-10-23 10:00:02'),
	(369, 25, 793, '52-TRIAL-TAUSA             53', '33-TRIAL-0  229', '8', '2016-10-23 10:00:07'),
	(370, 25, 797, '153-TRIAL-TENA             283', '20-TRIAL-0  214', '1', '2016-10-23 10:00:11'),
	(371, 25, 799, '163-TRIAL-TENJO            29', '252-TRIAL-0 264', '2', '2016-10-23 10:00:14'),
	(372, 25, 805, '5-TRIAL-TIBACUY            47', '94-TRIAL-0  187', '2', '2016-10-23 10:00:18'),
	(373, 25, 807, '23-TRIAL-TIBIRITA          240', '179-TRIAL-0 290', '1', '2016-10-23 10:00:22'),
	(374, 25, 815, '171-TRIAL-TOCAIMA          245', '121-TRIAL-0 270', '8', '2016-10-23 10:00:26'),
	(375, 25, 817, '255-TRIAL-TOCANCIPA        178', '179-TRIAL-0 206', '6', '2016-10-23 10:00:30'),
	(376, 25, 823, '87-TRIAL-TOPAIPI           196', '233-TRIAL-0 288', '3', '2016-10-23 10:00:34'),
	(377, 25, 839, '93-TRIAL-UBALA             90', '62-TRIAL-0  65', '2', '2016-10-23 10:00:38'),
	(378, 25, 841, '107-TRIAL-UBAQUE           267', '269-TRIAL-0 134', '2', '2016-10-23 10:00:42'),
	(379, 25, 843, '98-TRIAL-UBATE             245', '97-TRIAL-0  118', '1', '2016-10-23 10:00:46'),
	(380, 25, 845, '172-TRIAL-UNE              163', '28-TRIAL-0  164', '1', '2016-10-23 10:00:51'),
	(381, 25, 851, '290-TRIAL-UTICA            104', '101-TRIAL-0 227', '9', '2016-10-23 10:00:55'),
	(382, 25, 862, '271-TRIAL-VERGARA          263', '201-TRIAL-0 163', '2', '2016-10-23 10:01:00'),
	(383, 25, 871, '21-TRIAL-VILLAGOMEZ        45', '10-TRIAL-0  195', '2', '2016-10-23 10:01:04'),
	(384, 25, 873, '12-TRIAL-VILLAPINZON       51', '215-TRIAL-0 255', '9', '2016-10-23 10:01:08'),
	(385, 25, 875, '279-TRIAL-VILLETA          82', '108-TRIAL-0 54', '2', '2016-10-23 10:01:12'),
	(386, 25, 878, '245-TRIAL-VIOTA            138', '144-TRIAL-0 90', '1', '2016-10-23 10:01:16'),
	(387, 25, 885, '4-TRIAL-YACOPI             123', '25-TRIAL-0  278', '1', '2016-10-23 10:01:19'),
	(388, 25, 898, '230-TRIAL-ZIPACON          33', '23-TRIAL-0  194', '3', '2016-10-23 10:01:24'),
	(389, 25, 899, '187-TRIAL-ZIPAQUIRA        45', '205-TRIAL-0 216', '5', '2016-10-23 10:01:27'),
	(390, 25, 901, '38-TRIAL-ARBELAEZ          63', '235-TRIAL-0 297', '1', '2016-10-23 10:01:31'),
	(391, 27, 1, '124-TRIAL-QUIBDO           108', '37-TRIAL-0  174', '1', '2016-10-23 10:01:36'),
	(392, 27, 6, '93-TRIAL-ACANDI            255', '134-TRIAL-0 261', '1', '2016-10-23 10:01:40'),
	(393, 27, 25, '84-TRIAL-ALTO BAUDIO       175', '182-TRIAL-0 19', '1', '2016-10-23 10:01:44'),
	(394, 27, 73, '84-TRIAL-BAGADO            179', '279-TRIAL-0 283', '2', '2016-10-23 10:01:50'),
	(395, 27, 75, '125-TRIAL-BAHIA SOLANO     118', '137-TRIAL-0 28', '1', '2016-10-23 10:01:54'),
	(396, 27, 77, '237-TRIAL-BAJO BAUDO       291', '156-TRIAL-0 295', '1', '2016-10-23 10:01:59'),
	(397, 27, 99, '93-TRIAL-BOJAYA            132', '36-TRIAL-0  80', '1', '2016-10-23 10:02:03'),
	(398, 27, 135, '184-TRIAL-CANTON DEL SAN P 174', '19-TRIAL-0  90', '7', '2016-10-23 10:02:07'),
	(399, 27, 205, '51-TRIAL-CONDOTO           229', '90-TRIAL-0  174', '2', '2016-10-23 10:02:11'),
	(400, 27, 361, '189-TRIAL-ITSMINA          187', '290-TRIAL-0 239', '1', '2016-10-23 10:02:14'),
	(401, 27, 372, '263-TRIAL-JURADO           181', '103-TRIAL-0 5', '1', '2016-10-23 10:02:18'),
	(402, 27, 413, '195-TRIAL-LLORO            139', '68-TRIAL-0  198', '8', '2016-10-23 10:02:23'),
	(403, 27, 450, '115-TRIAL-LITORAL DEL BAJO 21', '293-TRIAL-0 226', '1', '2016-10-23 10:02:28'),
	(404, 27, 491, '228-TRIAL-NOVITA           281', '99-TRIAL-0  278', '1', '2016-10-23 10:02:32'),
	(405, 27, 495, '243-TRIAL-NUQUI            234', '143-TRIAL-0 149', '2', '2016-10-23 10:02:36'),
	(406, 27, 615, '102-TRIAL-RIOSUCIO         241', '187-TRIAL-0 246', '2', '2016-10-23 10:02:40'),
	(407, 27, 660, '113-TRIAL-SAN JOSE DEL PAL 0', '16-TRIAL-0  190', '2', '2016-10-23 10:02:44'),
	(408, 27, 745, '26-TRIAL-SIPI              210', '77-TRIAL-0  282', '1', '2016-10-23 10:02:48'),
	(409, 27, 787, '5-TRIAL-TADO               74', '263-TRIAL-0 22', '9', '2016-10-23 10:02:52'),
	(410, 27, 800, '160-TRIAL-UNGUIA           38', '288-TRIAL-0 11', '4', '2016-10-23 10:02:56'),
	(411, 30, 189, '25-TRIAL-CIENAGA DE ORO    167', '289-TRIAL-0 42', '1', '2016-10-23 10:03:00'),
	(412, 41, 1, '202-TRIAL-NEIVA            217', '99-TRIAL-0  123', '1', '2016-10-23 10:03:05'),
	(413, 41, 13, '48-TRIAL-AGRADO            51', '70-TRIAL-0  236', '1', '2016-10-23 10:03:09'),
	(414, 41, 14, '56-TRIAL-LA PLATA          105', '131-TRIAL-0 262', '1', '2016-10-23 10:03:13'),
	(415, 41, 16, '56-TRIAL-AIPE              31', '295-TRIAL-0 244', '2', '2016-10-23 10:03:17'),
	(416, 41, 20, '88-TRIAL-ALGECIRAS         15', '250-TRIAL-0 19', '2', '2016-10-23 10:03:22'),
	(417, 41, 26, '283-TRIAL-ALTAMIRA         233', '275-TRIAL-0 42', '1', '2016-10-23 10:03:26'),
	(418, 41, 78, '254-TRIAL-BARAYA           78', '285-TRIAL-0 157', '1', '2016-10-23 10:03:30'),
	(419, 41, 132, '290-TRIAL-CAMPOALEGRE      203', '250-TRIAL-0 133', '2', '2016-10-23 10:03:34'),
	(420, 41, 206, '111-TRIAL-COLOMBIA         90', '38-TRIAL-0  41', '1', '2016-10-23 10:03:38'),
	(421, 41, 244, '252-TRIAL-ELIAS            58', '179-TRIAL-0 102', '2', '2016-10-23 10:03:42'),
	(422, 41, 298, '102-TRIAL-GARZON           268', '222-TRIAL-0 95', '1', '2016-10-23 10:03:46'),
	(423, 41, 306, '61-TRIAL-GIGANTE           242', '294-TRIAL-0 299', '8', '2016-10-23 10:03:51'),
	(424, 41, 319, '142-TRIAL-GUADALUPE        157', '241-TRIAL-0 47', '2', '2016-10-23 10:03:55'),
	(425, 41, 349, '180-TRIAL-HOBO             113', '264-TRIAL-0 210', '2', '2016-10-23 10:03:59'),
	(426, 41, 359, '273-TRIAL-SAN JOSE DE ISNO 204', '162-TRIAL-0 3', '2', '2016-10-23 10:04:03'),
	(427, 41, 378, '54-TRIAL-LA ARGENTINA      229', '52-TRIAL-0  274', '2', '2016-10-23 10:04:07'),
	(428, 41, 518, '144-TRIAL-PAICOL           44', '149-TRIAL-0 18', '6', '2016-10-23 10:04:11'),
	(429, 41, 530, '52-TRIAL-PALESTINA         273', '170-TRIAL-0 31', '1', '2016-10-23 10:04:15'),
	(430, 41, 531, '169-TRIAL-PALERMO          298', '298-TRIAL-0 203', '5', '2016-10-23 10:04:20'),
	(431, 41, 548, '153-TRIAL-PITAL            143', '122-TRIAL-0 188', '6', '2016-10-23 10:04:24'),
	(432, 41, 551, '250-TRIAL-PITALITO         222', '40-TRIAL-0  11', '1', '2016-10-23 10:04:29'),
	(433, 41, 668, '80-TRIAL-SAN AGUSTIN       177', '16-TRIAL-0  176', '2', '2016-10-23 10:04:33'),
	(434, 41, 676, '215-TRIAL-SANTA MARIA      148', '98-TRIAL-0  79', '1', '2016-10-23 10:04:37'),
	(435, 41, 770, '183-TRIAL-SUAZA            162', '2-TRIAL-0   62', '2', '2016-10-23 10:04:41'),
	(436, 41, 791, '122-TRIAL-TARQUI           273', '41-TRIAL-0  86', '8', '2016-10-23 10:04:45'),
	(437, 41, 797, '217-TRIAL-TESALIA          92', '198-TRIAL-0 167', '4', '2016-10-23 10:04:49'),
	(438, 41, 799, '189-TRIAL-TELLO            32', '119-TRIAL-0 181', '2', '2016-10-23 10:04:53'),
	(439, 41, 801, '128-TRIAL-TERUEL           31', '189-TRIAL-0 52', '3', '2016-10-23 10:04:57'),
	(440, 41, 807, '64-TRIAL-TIMANA            107', '295-TRIAL-0 228', '2', '2016-10-23 10:05:01'),
	(441, 41, 872, '210-TRIAL-VILLAVIEJA       77', '171-TRIAL-0 36', '1', '2016-10-23 10:05:05'),
	(442, 41, 885, '40-TRIAL-YAGUARA           33', '37-TRIAL-0  193', '1', '2016-10-23 10:05:09'),
	(443, 44, 1, '93-TRIAL-RIOHACHA          282', '103-TRIAL-0 61', '1', '2016-10-23 10:05:14'),
	(444, 44, 78, '107-TRIAL-BARRANCAS        297', '251-TRIAL-0 123', '1', '2016-10-23 10:05:19'),
	(445, 44, 110, '40-TRIAL-ALBANIA           203', '166-TRIAL-0 83', '2', '2016-10-23 10:05:25'),
	(446, 44, 279, '245-TRIAL-FONSECA          286', '251-TRIAL-0 65', '1', '2016-10-23 10:05:29'),
	(447, 44, 430, '109-TRIAL-MAICAO           297', '283-TRIAL-0 150', '5', '2016-10-23 10:05:33'),
	(448, 44, 847, '175-TRIAL-URIBIA           281', '87-TRIAL-0  271', '8', '2016-10-23 10:05:37'),
	(449, 44, 855, '204-TRIAL-URUMITA          79', '38-TRIAL-0  200', '2', '2016-10-23 10:05:42'),
	(450, 44, 874, '118-TRIAL-VILLANUEVA       205', '40-TRIAL-0  228', '1', '2016-10-23 10:05:46'),
	(451, 44, 875, '298-TRIAL-SAN JUAN DEL CES 160', '98-TRIAL-0  35', '2', '2016-10-23 10:05:50'),
	(452, 44, 876, '67-TRIAL-EL MOLINO         193', '96-TRIAL-0  261', '2', '2016-10-23 10:05:55'),
	(453, 47, 1, '170-TRIAL-SANTA MARTA      138', '176-TRIAL-0 255', '1', '2016-10-23 10:05:59');
/*!40000 ALTER TABLE `gn_municipios` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_pais
DROP TABLE IF EXISTS `gn_pais`;
CREATE TABLE IF NOT EXISTS `gn_pais` (
  `codPais` int(11) NOT NULL,
  `nomPais` varchar(30) COLLATE utf8_bin NOT NULL DEFAULT 'NULL',
  `usuActividad` int(11) NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codPais`),
  UNIQUE KEY `Gn_Pais_gn_pais$codPais_UNIQUE` (`codPais`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_pais: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_pais` DISABLE KEYS */;
INSERT INTO `gn_pais` (`codPais`, `nomPais`, `usuActividad`, `tipActividad`, `horActividad`) VALUES
	(169, 'COLOMBIA', 0, 'A', '2016-08-02 19:48:53');
/*!40000 ALTER TABLE `gn_pais` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_permisos
DROP TABLE IF EXISTS `gn_permisos`;
CREATE TABLE IF NOT EXISTS `gn_permisos` (
  `IdPermiso` int(11) NOT NULL,
  `nombrePermiso` char(30) COLLATE utf8_bin NOT NULL,
  `url` char(50) COLLATE utf8_bin NOT NULL,
  `permisoPadre` int(11) DEFAULT NULL,
  `usuActividad` int(11) NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`IdPermiso`),
  KEY `FK_Gn_Permisos_Gn_Permisos` (`permisoPadre`),
  CONSTRAINT `FK_Gn_Permisos_Gn_Permisos` FOREIGN KEY (`permisoPadre`) REFERENCES `gn_permisos` (`IdPermiso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_permisos: ~101 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_permisos` DISABLE KEYS */;
INSERT INTO `gn_permisos` (`IdPermiso`, `nombrePermiso`, `url`, `permisoPadre`, `usuActividad`, `tipActividad`, `horActividad`) VALUES
	(1, 'Usuarios', '/biTercero/', NULL, 1068180, 'A', '2017-03-04 19:04:00'),
	(2, 'Beneficio', '/beBenficio/', NULL, 1068180, 'A', '2017-03-04 19:28:18'),
	(3, 'Solicitud', '/beSolicitud/', NULL, 1068180, 'A', '2017-03-04 19:34:05'),
	(4, 'Tipo de Beneficio', '/beTipobeneficio/', NULL, 1068180, 'A', '2017-03-04 19:38:17'),
	(5, 'Tipo de Servicio', '/beTiposervicio/', NULL, 1068180, 'A', '2017-03-04 19:46:30'),
	(6, 'Contrato Proveedor', '/biContratoproveedor/', NULL, 1068180, 'A', '2017-03-04 19:58:50'),
	(7, 'Contrato Empleados', '/biContratosempleado/', NULL, 1068180, 'A', '2017-03-04 20:08:10'),
	(8, 'Sueldo Empleado', '/biSueldoempleado', NULL, 1068180, 'A', '2017-03-04 20:28:47'),
	(9, 'Terceros', '/biTercero/', NULL, 1068180, 'A', '2017-03-04 21:09:03'),
	(11, 'Consultar Usuarios', '/biTercero/List', 1, 1068180, 'A', '2017-03-04 19:08:56'),
	(12, 'Editar Usuarios', '/biTercero/Edit', 1, 1068180, 'A', '2017-03-04 19:11:07'),
	(13, 'Crear Usuario', '/biTercero/Create', 1, 1068180, 'A', '2017-03-04 19:12:14'),
	(14, 'Ver Usuario', '/biTercero/View', 1, 1068180, 'A', '2017-03-04 19:14:55'),
	(21, 'Consultar Beneficio', '/beBenficio/List', 2, 1068180, 'A', '2017-03-04 19:29:55'),
	(22, 'Editar Beneficio', '/beBenficio/Edit', 2, 1068180, 'A', '2017-03-04 19:30:33'),
	(23, 'Crear Beneficio', '/beBenficio/Create', 2, 1068180, 'A', '2017-03-04 19:31:14'),
	(24, 'Ver Beneficio', '/beBenficio/View', 2, 1068180, 'A', '2017-03-04 19:32:00'),
	(31, 'Consultar Solicitud', '/beSolicitud/List', 3, 1068180, 'A', '2017-03-04 19:35:10'),
	(32, 'Crear Solicitud', '/beSolicitud/Create', 3, 1068180, 'A', '2017-03-04 19:35:50'),
	(33, 'Editar Solicitud', 'app/beSolicitud/Edit', 3, 1068180, 'A', '2017-03-05 10:57:45'),
	(34, 'Ver Solicitud', '/beSolicitud/View', 3, 1068180, 'A', '2017-03-04 19:36:36'),
	(41, 'Listar Tipo de Beneficio', '/beTipobeneficio/List', 4, 1068180, 'A', '2017-03-04 19:39:26'),
	(42, 'Crear Tipo Beneficio', '/beTipobeneficio/Create', 4, 1068180, 'A', '2017-03-04 19:41:04'),
	(43, 'Editar Tipo Beneficio', '/beTipobeneficio/Edit', 4, 1068180, 'A', '2017-03-04 19:44:47'),
	(44, 'Ver Tipo Beneficio', '/beTipobeneficio/View', 4, 1068180, 'A', '2017-03-04 19:45:24'),
	(51, 'Listar Tipo Servicio', '/beTiposervicio/List', 5, 1068180, 'A', '2017-03-04 19:53:41'),
	(52, 'Editar Tipo Servicio', '/beTiposervicio/Edit', 5, 1068180, 'A', '2017-03-04 19:54:50'),
	(53, 'Crear Tipo Servicio', '/beTiposervicio/Create', 5, 1068180, 'A', '2017-03-04 19:55:57'),
	(54, 'Ver Tipo Servicio', '/beTiposervicio/View', 5, 1068180, 'A', '2017-03-04 19:57:03'),
	(61, 'Listar Contrato Proveedor', '/biContratoproveedor/List', 6, 1068180, 'A', '2017-03-04 19:59:53'),
	(62, 'Editar Contrato Proveedor', '/biContratoproveedor/Edit', 6, 1068180, 'A', '2017-03-04 20:00:27'),
	(63, 'Crear Contrato Proveedor', '/biContratoproveedor/Create', 6, 1068180, 'A', '2017-03-04 20:03:30'),
	(64, 'Ver Contrato Proveedor', '/biContratoproveedor/View', 6, 1068180, 'A', '2017-03-04 20:05:09'),
	(71, 'Listar Contrato Empleados', '/biContratosempleado/List', 7, 1068180, 'A', '2017-03-04 20:14:53'),
	(72, 'Editar Contrato Empleados', '/biContratosempleado/Edit', 7, 1068180, 'A', '2017-03-04 20:16:30'),
	(73, 'Crear Contrato Empleados', '/biContratosempleado/Create', 7, 1068180, 'A', '2017-03-04 20:19:46'),
	(74, 'Ver Contrato Empleados', '/biContratosempleado/View', 7, 1068180, 'A', '2017-03-04 20:20:52'),
	(81, 'Listar Sueldo Empleado', '/biSueldoempleado/List', 8, 1068180, 'A', '2017-03-04 20:58:14'),
	(82, 'Editar Sueldo Empleado', '/biSueldoempleado/Edit', 8, 1068180, 'A', '2017-03-04 20:58:54'),
	(83, 'Crear Sueldo Empleado', '/biSueldoempleado/Create', 8, 1068180, 'A', '2017-03-04 20:59:27'),
	(84, 'Ver Sueldo Empleado', '/biSueldoempleado/View', 8, 1068180, 'A', '2017-03-04 21:01:32'),
	(91, 'Consultar Terceros', '/biTercero/List', 9, 1068180, 'A', '2017-03-04 21:10:25'),
	(92, 'Editar Tercero', '/biTercero/Edit', 9, 1068180, 'A', '2017-03-04 21:10:49'),
	(93, 'Crear Tercero', '/biTercero/Create', 9, 1068180, 'A', '2017-03-04 21:12:08'),
	(94, 'Ver Tercero', '/biTercero/View', 9, 1068180, 'A', '2017-03-04 21:13:03'),
	(100, 'Consultar Auditoria', '/gnAuditoria/List', NULL, 1068180, 'A', '2017-03-04 22:23:41'),
	(101, 'Departamento', '/gnDepartamento/', NULL, 1068180, 'A', '2017-03-04 22:33:27'),
	(102, 'Listar Departamentos', '/gnDepartamento/List', 101, 1068180, 'A', '2017-03-04 22:35:04'),
	(103, 'Editar Departamentos', '/gnDepartamento/Edit', 101, 1068180, 'A', '2017-03-04 22:37:56'),
	(104, 'Crear Departamento', '/gnDepartamento/Create', 101, 1068180, 'A', '2017-03-04 22:38:44'),
	(105, 'Ver Departamento', '/gnDepartamento/View', 101, 1068180, 'A', '2017-03-04 22:40:46'),
	(200, 'Documento Adjunto', '/gnDocadjunto/', NULL, 1068180, 'A', '2017-03-04 22:42:08'),
	(201, 'Listar Documento Adjunto', '/gnDocadjunto/List', 200, 1068180, 'A', '2017-03-04 22:45:18'),
	(202, 'Editar Documento Adjunto', '/gnDocadjunto/Edit', 200, 1068180, 'A', '2017-03-04 22:46:02'),
	(203, 'Crear Documento Adjunto', '/gnDocadjunto/Create', 200, 1068180, 'A', '2017-03-04 22:47:28'),
	(204, 'Ver Documento Adjunto', '/gnDocadjunto/View', 200, 1068180, 'A', '2017-03-04 22:48:06'),
	(300, 'Municipio', '/gnMunicipio/', NULL, 1068180, 'A', '2017-03-04 22:48:58'),
	(301, 'Listar Municipio', '/gnMunicipio/List', 300, 1068180, 'A', '2017-03-04 22:49:15'),
	(302, 'Editar Municipio', '/gnMunicipio/Edit', 300, 1068180, 'A', '2017-03-04 22:49:50'),
	(303, 'Crear Municipio', '/gnMunicipio/Create', 300, 1068180, 'A', '2017-03-04 22:50:17'),
	(304, 'Ver Municipio', '/gnMunicipio/View', 300, 1068180, 'A', '2017-03-04 22:50:48'),
	(400, 'Paises', '/gnPais/', NULL, 1068180, 'A', '2017-03-04 22:51:42'),
	(401, 'Listar Paises', '/gnPais/List', 400, 1068180, 'A', '2017-03-04 22:52:24'),
	(402, 'Crear Pais', '/gnPais/Create', 400, 1068180, 'A', '2017-03-04 22:53:05'),
	(403, 'Editar Pais', '/gnPais/Edit', 400, 1068180, 'A', '2017-03-04 22:53:25'),
	(404, 'Ver Pais', '/gnPais/View', 400, 1068180, 'A', '2017-03-04 22:53:51'),
	(500, 'Permisos', '/gnPermiso/', NULL, 1068180, 'A', '2017-03-04 23:02:23'),
	(501, 'Consultar Permisos', '/gnPermiso/List', 500, 1068180, 'A', '2017-03-04 23:02:50'),
	(502, 'Editar Permisos', '/gnPermiso/Edit', 500, 1068180, 'A', '2017-03-04 23:06:21'),
	(503, 'Crear Permisos', '/gnPermiso/Create', 500, 1068180, 'A', '2017-03-04 23:07:39'),
	(504, 'Ver Permisos', '/gnPermiso/View', 500, 1068180, 'A', '2017-03-04 23:09:11'),
	(600, 'Roles', '/gnRol/', NULL, 1068180, 'A', '2017-03-04 23:11:02'),
	(601, 'Listar Roles', '/gnRol/List', 600, 1068180, 'A', '2017-03-04 23:11:53'),
	(602, 'Editar Roles', '/gnRol/Edit', 600, 1068180, 'A', '2017-03-04 23:13:49'),
	(603, 'Crear Roles', '/gnRol/Create', 600, 1068180, 'A', '2017-03-04 23:14:25'),
	(604, 'Ver Roles', '/gnRol/View', 600, 1068180, 'A', '2017-03-04 23:14:55'),
	(700, 'Tipo Beneficiario', '/gnTipobeneficiario/', NULL, 1068180, 'A', '2017-03-04 23:16:13'),
	(701, 'Listar Tipo Beneficio', '/gnTipobeneficiario/List', 700, 1068180, 'A', '2017-03-04 23:16:47'),
	(702, 'Crear Tipo Beneficio', '/gnTipobeneficiario/Create', 700, 1068180, 'A', '2017-03-04 23:18:41'),
	(703, 'Editar Tipo Beneficio', '/gnTipobeneficiario/Edit', 700, 1068180, 'A', '2017-03-04 23:19:25'),
	(704, 'Ver Tipo Beneficio', '/gnTipobeneficiario/View', 700, 1068180, 'A', '2017-03-04 23:19:57'),
	(800, 'Tipo Documento', '/gnTipodocumento/', NULL, 1068180, 'A', '2017-03-04 23:21:31'),
	(801, 'Listar Tipo Documento', '/gnTipodocumento/List', 800, 1068180, 'A', '2017-03-04 23:22:17'),
	(802, 'Editar Tipo Documento', '/gnTipodocumento/Edit', 800, 1068180, 'A', '2017-03-04 23:23:11'),
	(803, 'Crear Tipo Documento', '/gnTipodocumento/Create', 800, 1068180, 'A', '2017-03-04 23:23:49'),
	(804, 'Ver Tipo Documento', '/gnTipodocumento/view', 800, 1068180, 'A', '2017-03-04 23:24:25'),
	(900, 'Tipo Identificacion', '/gnTipoidentificacion/', NULL, 1068180, 'A', '2017-03-04 23:28:36'),
	(901, 'Listar Tipo Identificacion', '/gnTipoidentificacion/List', 900, 1068180, 'A', '2017-03-04 23:29:36'),
	(902, 'Editar Tipo Identificacion', '/gnTipoidentificacion/Edit', 900, 1068180, 'A', '2017-03-04 23:30:25'),
	(903, 'Crear Tipo Identificacion', '/gnTipoidentificacion/Create', 900, 1068180, 'A', '2017-03-04 23:31:07'),
	(904, 'Ver Tipo Identificacion', '/gnTipoidentificacion/View', 900, 1068180, 'A', '2017-03-04 23:31:41'),
	(1000, 'Asignacion Presupuesto', '/peAsignacionpresupuesto/', NULL, 1068180, 'A', '2017-03-04 23:33:39'),
	(1001, 'Listar Asignacion Presupuesto', '/peAsignacionpresupuesto/List', 1000, 1068180, 'A', '2017-03-04 23:34:33'),
	(1002, 'Crear Asignacion Presupuesto', '/peAsignacionpresupuesto/Create', 1000, 1068180, 'A', '2017-03-04 23:35:17'),
	(1003, 'Editar Asignacion Presupuesto', '/peAsignacionpresupuesto/Edit', 1000, 1068180, 'A', '2017-03-04 23:38:41'),
	(1004, 'Ver Asignacion Presupuesto', '/peAsignacionpresupuesto/View', 1000, 1068180, 'A', '2017-03-04 23:39:22'),
	(1010, 'Detalle Presupuesto', '/peDetallepresupuesto', NULL, 1068180, 'A', '2017-03-04 23:41:23'),
	(1011, 'Listar Detalle Presupuesto', '/peDetallepresupuesto/List', 1010, 1068180, 'A', '2017-03-04 23:43:01'),
	(1012, 'Crear Detalle Presupuesto', '/peDetallepresupuesto/Create', 1010, 1068180, 'A', '2017-03-04 23:43:49'),
	(1013, 'Editar Detalle Presupuesto', '/peDetallepresupuesto/Edit', 1010, 1068180, 'A', '2017-03-04 23:44:59'),
	(1014, 'Ver Detalle Presupuesto', '/peDetallepresupuesto/View', 1010, 1068180, 'A', '2017-03-04 23:46:09');
/*!40000 ALTER TABLE `gn_permisos` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_permisos_gn_roles
DROP TABLE IF EXISTS `gn_permisos_gn_roles`;
CREATE TABLE IF NOT EXISTS `gn_permisos_gn_roles` (
  `idRol` int(11) NOT NULL,
  `idPermiso` int(11) NOT NULL,
  PRIMARY KEY (`idRol`,`idPermiso`),
  KEY `FK_Gn_permisos_Gn_Roles_Gn_Permisos` (`idPermiso`),
  CONSTRAINT `FK_Gn_permisos_Gn_Roles_Gn_Permisos` FOREIGN KEY (`idPermiso`) REFERENCES `gn_permisos` (`IdPermiso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Gn_permisos_Gn_Roles_Gn_Roles` FOREIGN KEY (`idRol`) REFERENCES `gn_roles` (`codRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_permisos_gn_roles: ~202 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_permisos_gn_roles` DISABLE KEYS */;
INSERT INTO `gn_permisos_gn_roles` (`idRol`, `idPermiso`) VALUES
	(2, 1),
	(3, 1),
	(2, 2),
	(3, 2),
	(1, 3),
	(2, 3),
	(3, 3),
	(2, 4),
	(3, 4),
	(2, 5),
	(3, 5),
	(2, 6),
	(3, 6),
	(2, 7),
	(3, 7),
	(2, 8),
	(3, 8),
	(2, 9),
	(3, 9),
	(2, 11),
	(3, 11),
	(2, 12),
	(3, 12),
	(3, 13),
	(2, 14),
	(3, 14),
	(2, 21),
	(3, 21),
	(2, 22),
	(3, 22),
	(2, 23),
	(3, 23),
	(2, 24),
	(3, 24),
	(2, 31),
	(3, 31),
	(2, 32),
	(3, 32),
	(2, 33),
	(3, 33),
	(1, 34),
	(2, 34),
	(3, 34),
	(2, 41),
	(3, 41),
	(2, 42),
	(3, 42),
	(2, 43),
	(3, 43),
	(2, 44),
	(3, 44),
	(2, 51),
	(3, 51),
	(2, 52),
	(3, 52),
	(2, 53),
	(3, 53),
	(2, 54),
	(3, 54),
	(2, 61),
	(3, 61),
	(2, 62),
	(3, 62),
	(2, 63),
	(3, 63),
	(2, 64),
	(3, 64),
	(2, 71),
	(3, 71),
	(2, 72),
	(3, 72),
	(2, 73),
	(3, 73),
	(2, 74),
	(3, 74),
	(2, 81),
	(3, 81),
	(2, 82),
	(3, 82),
	(2, 83),
	(3, 83),
	(2, 84),
	(3, 84),
	(2, 91),
	(3, 91),
	(2, 92),
	(3, 92),
	(2, 93),
	(3, 93),
	(1, 94),
	(2, 94),
	(3, 94),
	(2, 100),
	(3, 100),
	(2, 101),
	(3, 101),
	(2, 102),
	(3, 102),
	(2, 103),
	(3, 103),
	(2, 104),
	(3, 104),
	(2, 105),
	(3, 105),
	(2, 200),
	(3, 200),
	(2, 201),
	(3, 201),
	(2, 202),
	(3, 202),
	(2, 203),
	(3, 203),
	(2, 204),
	(3, 204),
	(2, 300),
	(3, 300),
	(2, 301),
	(3, 301),
	(2, 302),
	(3, 302),
	(2, 303),
	(3, 303),
	(2, 304),
	(3, 304),
	(2, 400),
	(3, 400),
	(2, 401),
	(3, 401),
	(2, 402),
	(3, 402),
	(2, 403),
	(3, 403),
	(2, 404),
	(3, 404),
	(2, 500),
	(3, 500),
	(2, 501),
	(3, 501),
	(2, 502),
	(3, 502),
	(2, 503),
	(3, 503),
	(2, 504),
	(3, 504),
	(2, 600),
	(3, 600),
	(2, 601),
	(3, 601),
	(2, 602),
	(3, 602),
	(2, 603),
	(3, 603),
	(2, 604),
	(3, 604),
	(2, 700),
	(3, 700),
	(2, 701),
	(3, 701),
	(2, 702),
	(3, 702),
	(2, 703),
	(3, 703),
	(2, 704),
	(3, 704),
	(2, 800),
	(3, 800),
	(2, 801),
	(3, 801),
	(2, 802),
	(3, 802),
	(2, 803),
	(3, 803),
	(2, 804),
	(3, 804),
	(2, 900),
	(3, 900),
	(2, 902),
	(3, 902),
	(2, 903),
	(3, 903),
	(2, 904),
	(3, 904),
	(2, 1000),
	(3, 1000),
	(2, 1001),
	(3, 1001),
	(2, 1002),
	(3, 1002),
	(2, 1003),
	(3, 1003),
	(2, 1004),
	(3, 1004),
	(2, 1010),
	(3, 1010),
	(2, 1011),
	(3, 1011),
	(2, 1012),
	(3, 1012),
	(2, 1013),
	(3, 1013),
	(2, 1014),
	(3, 1014);
/*!40000 ALTER TABLE `gn_permisos_gn_roles` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_roles
DROP TABLE IF EXISTS `gn_roles`;
CREATE TABLE IF NOT EXISTS `gn_roles` (
  `codRol` int(11) NOT NULL,
  `nomRol` varchar(50) COLLATE utf8_bin NOT NULL,
  `TipRol` char(10) COLLATE utf8_bin NOT NULL,
  `usuActividad` int(11) NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codRol`),
  UNIQUE KEY `Gn_Roles_gn_roles$codRoll_UNIQUE` (`codRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_roles: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_roles` DISABLE KEYS */;
INSERT INTO `gn_roles` (`codRol`, `nomRol`, `TipRol`, `usuActividad`, `tipActividad`, `horActividad`) VALUES
	(1, 'EMPLEADO', 'E', 0, 'A', '2016-08-04 15:44:47'),
	(2, 'USUARIO FUNCIONAL', 'C', 0, 'A', '2016-08-04 15:44:47'),
	(3, 'ADMINISTRADOR', 'D', 0, 'A', '2016-08-04 15:44:47'),
	(4, 'PROVEEDOR', 'P', 0, 'A', '2016-08-04 15:44:47');
/*!40000 ALTER TABLE `gn_roles` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_rol_bi_tercero
DROP TABLE IF EXISTS `gn_rol_bi_tercero`;
CREATE TABLE IF NOT EXISTS `gn_rol_bi_tercero` (
  `CodTerceros` int(11) NOT NULL,
  `CodRol` int(11) NOT NULL,
  PRIMARY KEY (`CodTerceros`,`CodRol`),
  KEY `FK_Gn_Rol_Bi_Tercero_Gn_Roles` (`CodRol`),
  CONSTRAINT `FK_Gn_Rol_Bi_Tercero_Bi_Terceros` FOREIGN KEY (`CodTerceros`) REFERENCES `bi_terceros` (`codTercero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_Gn_Rol_Bi_Tercero_Gn_Roles` FOREIGN KEY (`CodRol`) REFERENCES `gn_roles` (`codRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_rol_bi_tercero: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_rol_bi_tercero` DISABLE KEYS */;
INSERT INTO `gn_rol_bi_tercero` (`CodTerceros`, `CodRol`) VALUES
	(1068180, 3);
/*!40000 ALTER TABLE `gn_rol_bi_tercero` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_tipobeneficiarios
DROP TABLE IF EXISTS `gn_tipobeneficiarios`;
CREATE TABLE IF NOT EXISTS `gn_tipobeneficiarios` (
  `codTipoBeneficiarios` int(11) NOT NULL,
  `nomTipoBeneficiario` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `cantidadBemeficiario` int(11) DEFAULT NULL,
  `usuActividad` int(11) DEFAULT NULL,
  `tipActividad` char(1) COLLATE utf8_bin DEFAULT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codTipoBeneficiarios`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_tipobeneficiarios: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_tipobeneficiarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_tipobeneficiarios` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_tipodocumentos
DROP TABLE IF EXISTS `gn_tipodocumentos`;
CREATE TABLE IF NOT EXISTS `gn_tipodocumentos` (
  `codTipoDocumentos` int(11) NOT NULL,
  `nomDocumento` int(11) NOT NULL,
  `usuActividad` int(11) NOT NULL,
  `tipActividad` char(1) COLLATE utf8_bin NOT NULL,
  `horActividad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codTipoDocumentos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_tipodocumentos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_tipodocumentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `gn_tipodocumentos` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.gn_tipoidentificaciones
DROP TABLE IF EXISTS `gn_tipoidentificaciones`;
CREATE TABLE IF NOT EXISTS `gn_tipoidentificaciones` (
  `codTipoIdentificacion` int(11) NOT NULL AUTO_INCREMENT,
  `nomTipoIdentificacion` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`codTipoIdentificacion`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Volcando datos para la tabla sipre.gn_tipoidentificaciones: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `gn_tipoidentificaciones` DISABLE KEYS */;
INSERT INTO `gn_tipoidentificaciones` (`codTipoIdentificacion`, `nomTipoIdentificacion`) VALUES
	(1, 'NIT'),
	(2, 'RUT'),
	(3, 'Cedula ciudadania'),
	(5, 'Cedula extranjeria'),
	(6, 'Tarjeta identidad'),
	(7, 'Registro Civil');
/*!40000 ALTER TABLE `gn_tipoidentificaciones` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.pe_asignacionpresupuestos
DROP TABLE IF EXISTS `pe_asignacionpresupuestos`;
CREATE TABLE IF NOT EXISTS `pe_asignacionpresupuestos` (
  `codPresupuesto` int(11) NOT NULL,
  `codTipoBeneficio` int(11) NOT NULL,
  `anoPresupuesto` int(11) DEFAULT NULL,
  `valInicial` double DEFAULT NULL,
  `valEjecutado` double DEFAULT NULL,
  `valPendiente` double DEFAULT NULL,
  `usoActividad` int(11) DEFAULT NULL,
  `tipActividad` char(1) DEFAULT NULL,
  `horActivdad` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`codPresupuesto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sipre.pe_asignacionpresupuestos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pe_asignacionpresupuestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pe_asignacionpresupuestos` ENABLE KEYS */;

-- Volcando estructura para tabla sipre.pe_detallepresupuesto
DROP TABLE IF EXISTS `pe_detallepresupuesto`;
CREATE TABLE IF NOT EXISTS `pe_detallepresupuesto` (
  `codDetallePresupuesto` int(11) NOT NULL AUTO_INCREMENT,
  `codPresupuesto` int(11) DEFAULT NULL,
  `codSolicitud` int(11) DEFAULT NULL,
  PRIMARY KEY (`codDetallePresupuesto`),
  KEY `FK_detalle_presupuesto` (`codPresupuesto`),
  KEY `FK_solicitudes_presu` (`codSolicitud`),
  CONSTRAINT `FK_solicitudes_presu` FOREIGN KEY (`codSolicitud`) REFERENCES `be_solicitud` (`codSolicitud`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla sipre.pe_detallepresupuesto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pe_detallepresupuesto` DISABLE KEYS */;
/*!40000 ALTER TABLE `pe_detallepresupuesto` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;



update gn_permisos set url='/app/biTercero/' where idPermiso=1;
update gn_permisos set url='/app/beBenficio/' where idPermiso=2;
update gn_permisos set url='/app/beSolicitud/' where idPermiso=3;
update gn_permisos set url='/app/beTipobeneficio/' where idPermiso=4;
update gn_permisos set url='/app/beTiposervicio/' where idPermiso=5;
update gn_permisos set url='/app/biContratoproveedor/' where idPermiso=6;
update gn_permisos set url='/app/biContratosempleado/' where idPermiso=7;
update gn_permisos set url='/app/biSueldoempleado' where idPermiso=8;
update gn_permisos set url='/app/biTercero/' where idPermiso=9;
update gn_permisos set url='/app/biTercero/' where idPermiso=11;
update gn_permisos set url='/app/biTercero/Edit' where idPermiso=12;
update gn_permisos set url='/app/biTercero/Create' where idPermiso=13;
update gn_permisos set url='/app/biTercero/View' where idPermiso=14;
update gn_permisos set url='/app/beBenficio/List' where idPermiso=21;
update gn_permisos set url='/app/beBenficio/Edit' where idPermiso=22;
update gn_permisos set url='/app/beBenficio/Create' where idPermiso=23;
update gn_permisos set url='/app/beBenficio/View' where idPermiso=24;
update gn_permisos set url='/app/beSolicitud/List' where idPermiso=31;
update gn_permisos set url='/app/beSolicitud/Create' where idPermiso=32;
update gn_permisos set url='/appapp/beSolicitud/Edit' where idPermiso=33;
update gn_permisos set url='/app/beSolicitud/View' where idPermiso=34;
update gn_permisos set url='/app/beTipobeneficio/List' where idPermiso=41;
update gn_permisos set url='/app/beTipobeneficio/Create' where idPermiso=42;
update gn_permisos set url='/app/beTipobeneficio/Edit' where idPermiso=43;
update gn_permisos set url='/app/beTipobeneficio/View' where idPermiso=44;
update gn_permisos set url='/app/beTiposervicio/List' where idPermiso=51;
update gn_permisos set url='/app/beTiposervicio/Edit' where idPermiso=52;
update gn_permisos set url='/app/beTiposervicio/Create' where idPermiso=53;
update gn_permisos set url='/app/beTiposervicio/View' where idPermiso=54;
update gn_permisos set url='/app/biContratoproveedor/List' where idPermiso=61;
update gn_permisos set url='/app/biContratoproveedor/Edit' where idPermiso=62;
update gn_permisos set url='/app/biContratoproveedor/Create' where idPermiso=63;
update gn_permisos set url='/app/biContratoproveedor/View' where idPermiso=64;
update gn_permisos set url='/app/biContratosempleado/List' where idPermiso=71;
update gn_permisos set url='/app/biContratosempleado/Edit' where idPermiso=72;
update gn_permisos set url='/app/biContratosempleado/Create' where idPermiso=73;
update gn_permisos set url='/app/biContratosempleado/View' where idPermiso=74;
update gn_permisos set url='/app/biSueldoempleado/List' where idPermiso=81;
update gn_permisos set url='/app/biSueldoempleado/Edit' where idPermiso=82;
update gn_permisos set url='/app/biSueldoempleado/Create' where idPermiso=83;
update gn_permisos set url='/app/biSueldoempleado/View' where idPermiso=84;
update gn_permisos set url='/app/biTercero/List' where idPermiso=91;
update gn_permisos set url='/app/biTercero/Edit' where idPermiso=92;
update gn_permisos set url='/app/biTercero/Create' where idPermiso=93;
update gn_permisos set url='/app/biTercero/View' where idPermiso=94;
update gn_permisos set url='/app/gnAuditoria/List' where idPermiso=100;
update gn_permisos set url='/app/gnDepartamento/' where idPermiso=101;
update gn_permisos set url='/app/gnDepartamento/List' where idPermiso=102;
update gn_permisos set url='/app/gnDepartamento/Edit' where idPermiso=103;
update gn_permisos set url='/app/gnDepartamento/Create' where idPermiso=104;
update gn_permisos set url='/app/gnDepartamento/View' where idPermiso=105;
update gn_permisos set url='/app/gnDocadjunto/' where idPermiso=200;
update gn_permisos set url='/app/gnDocadjunto/List' where idPermiso=201;
update gn_permisos set url='/app/gnDocadjunto/Edit' where idPermiso=202;
update gn_permisos set url='/app/gnDocadjunto/Create' where idPermiso=203;
update gn_permisos set url='/app/gnDocadjunto/View' where idPermiso=204;
update gn_permisos set url='/app/gnMunicipio/' where idPermiso=300;
update gn_permisos set url='/app/gnMunicipio/List' where idPermiso=301;
update gn_permisos set url='/app/gnMunicipio/Edit' where idPermiso=302;
update gn_permisos set url='/app/gnMunicipio/Create' where idPermiso=303;
update gn_permisos set url='/app/gnMunicipio/View' where idPermiso=304;
update gn_permisos set url='/app/gnPais/' where idPermiso=400;
update gn_permisos set url='/app/gnPais/List' where idPermiso=401;
update gn_permisos set url='/app/gnPais/Create' where idPermiso=402;
update gn_permisos set url='/app/gnPais/Edit' where idPermiso=403;
update gn_permisos set url='/app/gnPais/View' where idPermiso=404;
update gn_permisos set url='/app/gnPermiso/' where idPermiso=500;
update gn_permisos set url='/app/gnPermiso/List' where idPermiso=501;
update gn_permisos set url='/app/gnPermiso/Edit' where idPermiso=502;
update gn_permisos set url='/app/gnPermiso/Create' where idPermiso=503;
update gn_permisos set url='/app/gnPermiso/View' where idPermiso=504;
update gn_permisos set url='/app/gnRol/' where idPermiso=600;
update gn_permisos set url='/app/gnRol/List' where idPermiso=601;
update gn_permisos set url='/app/gnRol/Edit' where idPermiso=602;
update gn_permisos set url='/app/gnRol/Create' where idPermiso=603;
update gn_permisos set url='/app/gnRol/View' where idPermiso=604;
update gn_permisos set url='/app/gnTipobeneficiario/' where idPermiso=700;
update gn_permisos set url='/app/gnTipobeneficiario/List' where idPermiso=701;
update gn_permisos set url='/app/gnTipobeneficiario/Create' where idPermiso=702;
update gn_permisos set url='/app/gnTipobeneficiario/Edit' where idPermiso=703;
update gn_permisos set url='/app/gnTipobeneficiario/View' where idPermiso=704;
update gn_permisos set url='/app/gnTipodocumento/' where idPermiso=800;
update gn_permisos set url='/app/gnTipodocumento/List' where idPermiso=801;
update gn_permisos set url='/app/gnTipodocumento/Edit' where idPermiso=802;
update gn_permisos set url='/app/gnTipodocumento/Create' where idPermiso=803;
update gn_permisos set url='/app/gnTipodocumento/view' where idPermiso=804;
update gn_permisos set url='/app/gnTipoidentificacion/' where idPermiso=900;
update gn_permisos set url='/app/gnTipoidentificacion/List' where idPermiso=901;
update gn_permisos set url='/app/gnTipoidentificacion/Edit' where idPermiso=902;
update gn_permisos set url='/app/gnTipoidentificacion/Create' where idPermiso=903;
update gn_permisos set url='/app/gnTipoidentificacion/View' where idPermiso=904;
update gn_permisos set url='/app/peAsignacionpresupuesto/' where idPermiso=1000;
update gn_permisos set url='/app/peAsignacionpresupuesto/List' where idPermiso=1001;
update gn_permisos set url='/app/peAsignacionpresupuesto/Create' where idPermiso=1002;
update gn_permisos set url='/app/peAsignacionpresupuesto/Edit' where idPermiso=1003;
update gn_permisos set url='/app/peAsignacionpresupuesto/View' where idPermiso=1004;
update gn_permisos set url='/app/peDetallepresupuesto' where idPermiso=1010;
update gn_permisos set url='/app/peDetallepresupuesto/List' where idPermiso=1011;
update gn_permisos set url='/app/peDetallepresupuesto/Create' where idPermiso=1012;
update gn_permisos set url='/app/peDetallepresupuesto/Edit' where idPermiso=1013;
update gn_permisos set url='/app/peDetallepresupuesto/View' where idPermiso=1014;