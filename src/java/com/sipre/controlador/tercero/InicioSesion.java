/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.controlador.tercero;

import com.sipre.entidad.BiTercero;
import com.sipre.entidad.GnRol;
import com.sipre.facades.BiTerceroFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 *
 * @author AlejoZepol
 */
@Named(value = "inicioSesion")
@SessionScoped
public class InicioSesion implements Serializable {

    /**
     * Creates a new instance of BiTerceros
     */
    private BiTercero usuario;
    @EJB
    private BiTerceroFacade usuarioFacade;
    private GnRol rolSeleccionado;
    private String documento;
    private String clave;

    public InicioSesion() {
    }

          
    @PostConstruct
    public void init(){
        
    }
    public BiTercero getUsuario() {
        return usuario;
    }

    public void setUsuario(BiTercero usuario) {
        this.usuario = usuario;
    }

    public BiTerceroFacade getUsuarioFacade() {
        return usuarioFacade;
    }

    public void setUsuarioFacade(BiTerceroFacade usuarioFacade) {
        this.usuarioFacade = usuarioFacade;
    }

    public GnRol getRolSeleccionado() {
        return rolSeleccionado;
    }

    public void setRolSeleccionado(GnRol rolSeleccionado) {
        this.rolSeleccionado = rolSeleccionado;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
  
    

    public String iniciarSesion() {
        String url = "";
        usuario = usuarioFacade.login(documento, clave);
        FacesContext fc = FacesContext.getCurrentInstance();
        if (usuario != null) {
            rolSeleccionado = usuario.getGnRoles().get(0);
            url = "app/inicio.xhtml?faces-redirect=true";
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Datos invalidos", "Documento o clave incorrectos");
            FacesMessage messageClv = new FacesMessage(FacesMessage.SEVERITY_WARN,
                    "Clave invalida","Verifique por favor");
            fc.addMessage("doc", message);
            fc.addMessage("clv", messageClv);
        }
        return url;
    }

    public String cerrarSesion() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        documento = "";
        clave = "";
        usuario = null;
        return "/index.xhtml?faces-redirect=true";
    }

}


