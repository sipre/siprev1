/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "be_benficios")
@NamedQueries({
    @NamedQuery(name = "BeBenficio.findAll", query = "SELECT b FROM BeBenficio b")
    , @NamedQuery(name = "BeBenficio.findByCodBeneficio", query = "SELECT b FROM BeBenficio b WHERE b.codBeneficio = :codBeneficio")
    , @NamedQuery(name = "BeBenficio.findByNombreBeneficio", query = "SELECT b FROM BeBenficio b WHERE b.nombreBeneficio = :nombreBeneficio")
    , @NamedQuery(name = "BeBenficio.findByValor", query = "SELECT b FROM BeBenficio b WHERE b.valor = :valor")
    , @NamedQuery(name = "BeBenficio.findByDesEmpleados", query = "SELECT b FROM BeBenficio b WHERE b.desEmpleados = :desEmpleados")
    , @NamedQuery(name = "BeBenficio.findByPorEmpleado", query = "SELECT b FROM BeBenficio b WHERE b.porEmpleado = :porEmpleado")
    , @NamedQuery(name = "BeBenficio.findByPorEmpresa", query = "SELECT b FROM BeBenficio b WHERE b.porEmpresa = :porEmpresa")
    , @NamedQuery(name = "BeBenficio.findBySobreSMLV", query = "SELECT b FROM BeBenficio b WHERE b.sobreSMLV = :sobreSMLV")
    , @NamedQuery(name = "BeBenficio.findByPorSMLV", query = "SELECT b FROM BeBenficio b WHERE b.porSMLV = :porSMLV")
    , @NamedQuery(name = "BeBenficio.findByConFijo", query = "SELECT b FROM BeBenficio b WHERE b.conFijo = :conFijo")
    , @NamedQuery(name = "BeBenficio.findByConIndefinido", query = "SELECT b FROM BeBenficio b WHERE b.conIndefinido = :conIndefinido")
    , @NamedQuery(name = "BeBenficio.findByConSena", query = "SELECT b FROM BeBenficio b WHERE b.conSena = :conSena")
    , @NamedQuery(name = "BeBenficio.findByBeneficarios", query = "SELECT b FROM BeBenficio b WHERE b.beneficarios = :beneficarios")
    , @NamedQuery(name = "BeBenficio.findBySueBasico", query = "SELECT b FROM BeBenficio b WHERE b.sueBasico = :sueBasico")
    , @NamedQuery(name = "BeBenficio.findByActUsuario", query = "SELECT b FROM BeBenficio b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BeBenficio.findByIndActividad", query = "SELECT b FROM BeBenficio b WHERE b.indActividad = :indActividad")
    , @NamedQuery(name = "BeBenficio.findByActHora", query = "SELECT b FROM BeBenficio b WHERE b.actHora = :actHora")})
public class BeBenficio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CodBeneficio")
    private Integer codBeneficio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "NombreBeneficio")
    private String nombreBeneficio;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private Double valor;
    @Column(name = "desEmpleados")
    private Character desEmpleados;
    @Column(name = "porEmpleado")
    private BigDecimal porEmpleado;
    @Column(name = "porEmpresa")
    private BigDecimal porEmpresa;
    @Column(name = "sobreSMLV")
    private Character sobreSMLV;
    @Column(name = "porSMLV")
    private BigDecimal porSMLV;
    @Column(name = "conFijo")
    private Character conFijo;
    @Column(name = "conIndefinido")
    private Character conIndefinido;
    @Column(name = "conSena")
    private Character conSena;
    @Basic(optional = false)
    @NotNull
    @Column(name = "beneficarios")
    private Character beneficarios;
    @Column(name = "sueBasico")
    private Character sueBasico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actUsuario")
    private int actUsuario;
    @Size(max = 1)
    @Column(name = "indActividad")
    private String indActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actHora;
    @JoinColumn(name = "codTipoBeneficio", referencedColumnName = "codTipoBeneficio")
    @ManyToOne
    private BeTipobeneficio codTipoBeneficio;
    @OneToMany(mappedBy = "codBeneficio")
    private List<BeTiposervicio> beTiposervicios;

    public BeBenficio() {
    }

    public BeBenficio(Integer codBeneficio) {
        this.codBeneficio = codBeneficio;
    }

    public BeBenficio(Integer codBeneficio, String nombreBeneficio, Character beneficarios, int actUsuario, Date actHora) {
        this.codBeneficio = codBeneficio;
        this.nombreBeneficio = nombreBeneficio;
        this.beneficarios = beneficarios;
        this.actUsuario = actUsuario;
        this.actHora = actHora;
    }

    public Integer getCodBeneficio() {
        return codBeneficio;
    }

    public void setCodBeneficio(Integer codBeneficio) {
        this.codBeneficio = codBeneficio;
    }

    public String getNombreBeneficio() {
        return nombreBeneficio;
    }

    public void setNombreBeneficio(String nombreBeneficio) {
        this.nombreBeneficio = nombreBeneficio;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Character getDesEmpleados() {
        return desEmpleados;
    }

    public void setDesEmpleados(Character desEmpleados) {
        this.desEmpleados = desEmpleados;
    }

    public BigDecimal getPorEmpleado() {
        return porEmpleado;
    }

    public void setPorEmpleado(BigDecimal porEmpleado) {
        this.porEmpleado = porEmpleado;
    }

    public BigDecimal getPorEmpresa() {
        return porEmpresa;
    }

    public void setPorEmpresa(BigDecimal porEmpresa) {
        this.porEmpresa = porEmpresa;
    }

    public Character getSobreSMLV() {
        return sobreSMLV;
    }

    public void setSobreSMLV(Character sobreSMLV) {
        this.sobreSMLV = sobreSMLV;
    }

    public BigDecimal getPorSMLV() {
        return porSMLV;
    }

    public void setPorSMLV(BigDecimal porSMLV) {
        this.porSMLV = porSMLV;
    }

    public Character getConFijo() {
        return conFijo;
    }

    public void setConFijo(Character conFijo) {
        this.conFijo = conFijo;
    }

    public Character getConIndefinido() {
        return conIndefinido;
    }

    public void setConIndefinido(Character conIndefinido) {
        this.conIndefinido = conIndefinido;
    }

    public Character getConSena() {
        return conSena;
    }

    public void setConSena(Character conSena) {
        this.conSena = conSena;
    }

    public Character getBeneficarios() {
        return beneficarios;
    }

    public void setBeneficarios(Character beneficarios) {
        this.beneficarios = beneficarios;
    }

    public Character getSueBasico() {
        return sueBasico;
    }

    public void setSueBasico(Character sueBasico) {
        this.sueBasico = sueBasico;
    }

    public int getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(int actUsuario) {
        this.actUsuario = actUsuario;
    }

    public String getIndActividad() {
        return indActividad;
    }

    public void setIndActividad(String indActividad) {
        this.indActividad = indActividad;
    }

    public Date getActHora() {
        return actHora;
    }

    public void setActHora(Date actHora) {
        this.actHora = actHora;
    }

    public BeTipobeneficio getCodTipoBeneficio() {
        return codTipoBeneficio;
    }

    public void setCodTipoBeneficio(BeTipobeneficio codTipoBeneficio) {
        this.codTipoBeneficio = codTipoBeneficio;
    }

    public List<BeTiposervicio> getBeTiposervicios() {
        return beTiposervicios;
    }

    public void setBeTiposervicios(List<BeTiposervicio> beTiposervicios) {
        this.beTiposervicios = beTiposervicios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codBeneficio != null ? codBeneficio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BeBenficio)) {
            return false;
        }
        BeBenficio other = (BeBenficio) object;
        if ((this.codBeneficio == null && other.codBeneficio != null) || (this.codBeneficio != null && !this.codBeneficio.equals(other.codBeneficio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BeBenficio[ codBeneficio=" + codBeneficio + " ]";
    }
    
}
