/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "be_solicitud")
@NamedQueries({
    @NamedQuery(name = "BeSolicitud.findAll", query = "SELECT b FROM BeSolicitud b")
    , @NamedQuery(name = "BeSolicitud.findByCodSolicitud", query = "SELECT b FROM BeSolicitud b WHERE b.codSolicitud = :codSolicitud")
    , @NamedQuery(name = "BeSolicitud.findByEstSolicitud", query = "SELECT b FROM BeSolicitud b WHERE b.estSolicitud = :estSolicitud")
    , @NamedQuery(name = "BeSolicitud.findByFecSolicitud", query = "SELECT b FROM BeSolicitud b WHERE b.fecSolicitud = :fecSolicitud")
    , @NamedQuery(name = "BeSolicitud.findByObservaciones", query = "SELECT b FROM BeSolicitud b WHERE b.observaciones = :observaciones")
    , @NamedQuery(name = "BeSolicitud.findByValorEmpresa", query = "SELECT b FROM BeSolicitud b WHERE b.valorEmpresa = :valorEmpresa")
    , @NamedQuery(name = "BeSolicitud.findByNumfactura", query = "SELECT b FROM BeSolicitud b WHERE b.numfactura = :numfactura")
    , @NamedQuery(name = "BeSolicitud.findByValorEmpleado", query = "SELECT b FROM BeSolicitud b WHERE b.valorEmpleado = :valorEmpleado")
    , @NamedQuery(name = "BeSolicitud.findByUsuActividad", query = "SELECT b FROM BeSolicitud b WHERE b.usuActividad = :usuActividad")
    , @NamedQuery(name = "BeSolicitud.findByTipActividad", query = "SELECT b FROM BeSolicitud b WHERE b.tipActividad = :tipActividad")
    , @NamedQuery(name = "BeSolicitud.findByHorActividad", query = "SELECT b FROM BeSolicitud b WHERE b.horActividad = :horActividad")})
public class BeSolicitud implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codSolicitud")
    private Integer codSolicitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estSolicitud")
    private Character estSolicitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecSolicitud")
    @Temporal(TemporalType.DATE)
    private Date fecSolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "observaciones")
    private String observaciones;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valorEmpresa")
    private Double valorEmpresa;
    @Size(max = 25)
    @Column(name = "numfactura")
    private String numfactura;
    @Column(name = "valorEmpleado")
    private Double valorEmpleado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuActividad")
    private int usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @JoinColumn(name = "codTomador", referencedColumnName = "codTercero")
    @ManyToOne(optional = false)
    private BiTercero codTomador;
    @JoinColumn(name = "codtipoServicio", referencedColumnName = "codTipoServicio")
    @ManyToOne(optional = false)
    private BeTiposervicio codtipoServicio;
    @JoinColumn(name = "CodContratoSolicitante", referencedColumnName = "codContrato")
    @ManyToOne
    private BiContratosempleado codContratoSolicitante;
    @OneToMany(mappedBy = "codSolicitud")
    private List<PeDetallepresupuesto> peDetallepresupuestos;

    public BeSolicitud() {
    }

    public BeSolicitud(Integer codSolicitud) {
        this.codSolicitud = codSolicitud;
    }

    public BeSolicitud(Integer codSolicitud, Character estSolicitud, Date fecSolicitud, String observaciones, int usuActividad, Character tipActividad, Date horActividad) {
        this.codSolicitud = codSolicitud;
        this.estSolicitud = estSolicitud;
        this.fecSolicitud = fecSolicitud;
        this.observaciones = observaciones;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodSolicitud() {
        return codSolicitud;
    }

    public void setCodSolicitud(Integer codSolicitud) {
        this.codSolicitud = codSolicitud;
    }

    public Character getEstSolicitud() {
        return estSolicitud;
    }

    public void setEstSolicitud(Character estSolicitud) {
        this.estSolicitud = estSolicitud;
    }

    public Date getFecSolicitud() {
        return fecSolicitud;
    }

    public void setFecSolicitud(Date fecSolicitud) {
        this.fecSolicitud = fecSolicitud;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Double getValorEmpresa() {
        return valorEmpresa;
    }

    public void setValorEmpresa(Double valorEmpresa) {
        this.valorEmpresa = valorEmpresa;
    }

    public String getNumfactura() {
        return numfactura;
    }

    public void setNumfactura(String numfactura) {
        this.numfactura = numfactura;
    }

    public Double getValorEmpleado() {
        return valorEmpleado;
    }

    public void setValorEmpleado(Double valorEmpleado) {
        this.valorEmpleado = valorEmpleado;
    }

    public int getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(int usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public BiTercero getCodTomador() {
        return codTomador;
    }

    public void setCodTomador(BiTercero codTomador) {
        this.codTomador = codTomador;
    }

    public BeTiposervicio getCodtipoServicio() {
        return codtipoServicio;
    }

    public void setCodtipoServicio(BeTiposervicio codtipoServicio) {
        this.codtipoServicio = codtipoServicio;
    }

    public BiContratosempleado getCodContratoSolicitante() {
        return codContratoSolicitante;
    }

    public void setCodContratoSolicitante(BiContratosempleado codContratoSolicitante) {
        this.codContratoSolicitante = codContratoSolicitante;
    }

    public List<PeDetallepresupuesto> getPeDetallepresupuestos() {
        return peDetallepresupuestos;
    }

    public void setPeDetallepresupuestos(List<PeDetallepresupuesto> peDetallepresupuestos) {
        this.peDetallepresupuestos = peDetallepresupuestos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSolicitud != null ? codSolicitud.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BeSolicitud)) {
            return false;
        }
        BeSolicitud other = (BeSolicitud) object;
        if ((this.codSolicitud == null && other.codSolicitud != null) || (this.codSolicitud != null && !this.codSolicitud.equals(other.codSolicitud))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BeSolicitud[ codSolicitud=" + codSolicitud + " ]";
    }
    
}
