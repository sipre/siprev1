/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "be_tipobeneficio")
@NamedQueries({
    @NamedQuery(name = "BeTipobeneficio.findAll", query = "SELECT b FROM BeTipobeneficio b")
    , @NamedQuery(name = "BeTipobeneficio.findByCodTipoBeneficio", query = "SELECT b FROM BeTipobeneficio b WHERE b.codTipoBeneficio = :codTipoBeneficio")
    , @NamedQuery(name = "BeTipobeneficio.findByNombreBeneficio", query = "SELECT b FROM BeTipobeneficio b WHERE b.nombreBeneficio = :nombreBeneficio")
    , @NamedQuery(name = "BeTipobeneficio.findByPrescripcion", query = "SELECT b FROM BeTipobeneficio b WHERE b.prescripcion = :prescripcion")
    , @NamedQuery(name = "BeTipobeneficio.findByFactura", query = "SELECT b FROM BeTipobeneficio b WHERE b.factura = :factura")
    , @NamedQuery(name = "BeTipobeneficio.findByPresupuesto", query = "SELECT b FROM BeTipobeneficio b WHERE b.presupuesto = :presupuesto")
    , @NamedQuery(name = "BeTipobeneficio.findByUsuActividad", query = "SELECT b FROM BeTipobeneficio b WHERE b.usuActividad = :usuActividad")
    , @NamedQuery(name = "BeTipobeneficio.findByEstActividad", query = "SELECT b FROM BeTipobeneficio b WHERE b.estActividad = :estActividad")
    , @NamedQuery(name = "BeTipobeneficio.findByHorActividad", query = "SELECT b FROM BeTipobeneficio b WHERE b.horActividad = :horActividad")})
public class BeTipobeneficio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTipoBeneficio")
    private Integer codTipoBeneficio;
    @Size(max = 50)
    @Column(name = "NombreBeneficio")
    private String nombreBeneficio;
    @Column(name = "Prescripcion")
    private Character prescripcion;
    @Column(name = "factura")
    private Character factura;
    @Column(name = "presupuesto")
    private Character presupuesto;
    @Column(name = "usuActividad")
    private Integer usuActividad;
    @Column(name = "estActividad")
    private Character estActividad;
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(mappedBy = "codTipoBeneficio")
    private List<BeBenficio> beBenficios;

    public BeTipobeneficio() {
    }

    public BeTipobeneficio(Integer codTipoBeneficio) {
        this.codTipoBeneficio = codTipoBeneficio;
    }

    public Integer getCodTipoBeneficio() {
        return codTipoBeneficio;
    }

    public void setCodTipoBeneficio(Integer codTipoBeneficio) {
        this.codTipoBeneficio = codTipoBeneficio;
    }

    public String getNombreBeneficio() {
        return nombreBeneficio;
    }

    public void setNombreBeneficio(String nombreBeneficio) {
        this.nombreBeneficio = nombreBeneficio;
    }

    public Character getPrescripcion() {
        return prescripcion;
    }

    public void setPrescripcion(Character prescripcion) {
        this.prescripcion = prescripcion;
    }

    public Character getFactura() {
        return factura;
    }

    public void setFactura(Character factura) {
        this.factura = factura;
    }

    public Character getPresupuesto() {
        return presupuesto;
    }

    public void setPresupuesto(Character presupuesto) {
        this.presupuesto = presupuesto;
    }

    public Integer getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(Integer usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getEstActividad() {
        return estActividad;
    }

    public void setEstActividad(Character estActividad) {
        this.estActividad = estActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<BeBenficio> getBeBenficios() {
        return beBenficios;
    }

    public void setBeBenficios(List<BeBenficio> beBenficios) {
        this.beBenficios = beBenficios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoBeneficio != null ? codTipoBeneficio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BeTipobeneficio)) {
            return false;
        }
        BeTipobeneficio other = (BeTipobeneficio) object;
        if ((this.codTipoBeneficio == null && other.codTipoBeneficio != null) || (this.codTipoBeneficio != null && !this.codTipoBeneficio.equals(other.codTipoBeneficio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BeTipobeneficio[ codTipoBeneficio=" + codTipoBeneficio + " ]";
    }
    
}
