/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "be_tiposervicios")
@NamedQueries({
    @NamedQuery(name = "BeTiposervicio.findAll", query = "SELECT b FROM BeTiposervicio b")
    , @NamedQuery(name = "BeTiposervicio.findByCodTipoServicio", query = "SELECT b FROM BeTiposervicio b WHERE b.codTipoServicio = :codTipoServicio")
    , @NamedQuery(name = "BeTiposervicio.findByNomTipoServicio", query = "SELECT b FROM BeTiposervicio b WHERE b.nomTipoServicio = :nomTipoServicio")
    , @NamedQuery(name = "BeTiposervicio.findByIndActividad", query = "SELECT b FROM BeTiposervicio b WHERE b.indActividad = :indActividad")
    , @NamedQuery(name = "BeTiposervicio.findByActUsuario", query = "SELECT b FROM BeTiposervicio b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BeTiposervicio.findByEstActividad", query = "SELECT b FROM BeTiposervicio b WHERE b.estActividad = :estActividad")
    , @NamedQuery(name = "BeTiposervicio.findByActHora", query = "SELECT b FROM BeTiposervicio b WHERE b.actHora = :actHora")})
public class BeTiposervicio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTipoServicio")
    private Integer codTipoServicio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nomTipoServicio")
    private String nomTipoServicio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "indActividad")
    private Character indActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actUsuario")
    private int actUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estActividad")
    private Character estActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actHora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codtipoServicio")
    private List<BeSolicitud> beSolicitudes;
    @JoinColumn(name = "CodBeneficio", referencedColumnName = "CodBeneficio")
    @ManyToOne
    private BeBenficio codBeneficio;
    @JoinColumn(name = "CodContratoProveedor", referencedColumnName = "CodContrato")
    @ManyToOne
    private BiContratoproveedor codContratoProveedor;

    public BeTiposervicio() {
    }

    public BeTiposervicio(Integer codTipoServicio) {
        this.codTipoServicio = codTipoServicio;
    }

    public BeTiposervicio(Integer codTipoServicio, String nomTipoServicio, Character indActividad, int actUsuario, Character estActividad, Date actHora) {
        this.codTipoServicio = codTipoServicio;
        this.nomTipoServicio = nomTipoServicio;
        this.indActividad = indActividad;
        this.actUsuario = actUsuario;
        this.estActividad = estActividad;
        this.actHora = actHora;
    }

    public Integer getCodTipoServicio() {
        return codTipoServicio;
    }

    public void setCodTipoServicio(Integer codTipoServicio) {
        this.codTipoServicio = codTipoServicio;
    }

    public String getNomTipoServicio() {
        return nomTipoServicio;
    }

    public void setNomTipoServicio(String nomTipoServicio) {
        this.nomTipoServicio = nomTipoServicio;
    }

    public Character getIndActividad() {
        return indActividad;
    }

    public void setIndActividad(Character indActividad) {
        this.indActividad = indActividad;
    }

    public int getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(int actUsuario) {
        this.actUsuario = actUsuario;
    }

    public Character getEstActividad() {
        return estActividad;
    }

    public void setEstActividad(Character estActividad) {
        this.estActividad = estActividad;
    }

    public Date getActHora() {
        return actHora;
    }

    public void setActHora(Date actHora) {
        this.actHora = actHora;
    }

    public List<BeSolicitud> getBeSolicitudes() {
        return beSolicitudes;
    }

    public void setBeSolicitudes(List<BeSolicitud> beSolicitudes) {
        this.beSolicitudes = beSolicitudes;
    }

    public BeBenficio getCodBeneficio() {
        return codBeneficio;
    }

    public void setCodBeneficio(BeBenficio codBeneficio) {
        this.codBeneficio = codBeneficio;
    }

    public BiContratoproveedor getCodContratoProveedor() {
        return codContratoProveedor;
    }

    public void setCodContratoProveedor(BiContratoproveedor codContratoProveedor) {
        this.codContratoProveedor = codContratoProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoServicio != null ? codTipoServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BeTiposervicio)) {
            return false;
        }
        BeTiposervicio other = (BeTiposervicio) object;
        if ((this.codTipoServicio == null && other.codTipoServicio != null) || (this.codTipoServicio != null && !this.codTipoServicio.equals(other.codTipoServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BeTiposervicio[ codTipoServicio=" + codTipoServicio + " ]";
    }
    
}
