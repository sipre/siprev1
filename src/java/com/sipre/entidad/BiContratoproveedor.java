/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "bi_contratoproveedor")
@NamedQueries({
    @NamedQuery(name = "BiContratoproveedor.findAll", query = "SELECT b FROM BiContratoproveedor b")
    , @NamedQuery(name = "BiContratoproveedor.findByCodContrato", query = "SELECT b FROM BiContratoproveedor b WHERE b.codContrato = :codContrato")
    , @NamedQuery(name = "BiContratoproveedor.findByNumContrato", query = "SELECT b FROM BiContratoproveedor b WHERE b.numContrato = :numContrato")
    , @NamedQuery(name = "BiContratoproveedor.findByActUsuario", query = "SELECT b FROM BiContratoproveedor b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BiContratoproveedor.findByEstActividad", query = "SELECT b FROM BiContratoproveedor b WHERE b.estActividad = :estActividad")
    , @NamedQuery(name = "BiContratoproveedor.findByHorActividad", query = "SELECT b FROM BiContratoproveedor b WHERE b.horActividad = :horActividad")})
public class BiContratoproveedor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "CodContrato")
    private Integer codContrato;
    @Column(name = "numContrato")
    private Integer numContrato;
    @Column(name = "actUsuario")
    private Integer actUsuario;
    @Column(name = "estActividad")
    private Character estActividad;
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(mappedBy = "codContratoProveedor")
    private List<BeTiposervicio> beTiposervicios;
    @JoinColumn(name = "codProveedor", referencedColumnName = "codTercero")
    @ManyToOne
    private BiTercero codProveedor;

    public BiContratoproveedor() {
    }

    public BiContratoproveedor(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public Integer getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public Integer getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(Integer numContrato) {
        this.numContrato = numContrato;
    }

    public Integer getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(Integer actUsuario) {
        this.actUsuario = actUsuario;
    }

    public Character getEstActividad() {
        return estActividad;
    }

    public void setEstActividad(Character estActividad) {
        this.estActividad = estActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<BeTiposervicio> getBeTiposervicios() {
        return beTiposervicios;
    }

    public void setBeTiposervicios(List<BeTiposervicio> beTiposervicios) {
        this.beTiposervicios = beTiposervicios;
    }

    public BiTercero getCodProveedor() {
        return codProveedor;
    }

    public void setCodProveedor(BiTercero codProveedor) {
        this.codProveedor = codProveedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codContrato != null ? codContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BiContratoproveedor)) {
            return false;
        }
        BiContratoproveedor other = (BiContratoproveedor) object;
        if ((this.codContrato == null && other.codContrato != null) || (this.codContrato != null && !this.codContrato.equals(other.codContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BiContratoproveedor[ codContrato=" + codContrato + " ]";
    }
    
}
