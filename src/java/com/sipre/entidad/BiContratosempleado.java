/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "bi_contratosempleado")
@NamedQueries({
    @NamedQuery(name = "BiContratosempleado.findAll", query = "SELECT b FROM BiContratosempleado b")
    , @NamedQuery(name = "BiContratosempleado.findByCodContrato", query = "SELECT b FROM BiContratosempleado b WHERE b.codContrato = :codContrato")
    , @NamedQuery(name = "BiContratosempleado.findByTipContrato", query = "SELECT b FROM BiContratosempleado b WHERE b.tipContrato = :tipContrato")
    , @NamedQuery(name = "BiContratosempleado.findByFecInicio", query = "SELECT b FROM BiContratosempleado b WHERE b.fecInicio = :fecInicio")
    , @NamedQuery(name = "BiContratosempleado.findByFecFin", query = "SELECT b FROM BiContratosempleado b WHERE b.fecFin = :fecFin")
    , @NamedQuery(name = "BiContratosempleado.findByActUsuario", query = "SELECT b FROM BiContratosempleado b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BiContratosempleado.findByEstActividad", query = "SELECT b FROM BiContratosempleado b WHERE b.estActividad = :estActividad")
    , @NamedQuery(name = "BiContratosempleado.findByHorActividad", query = "SELECT b FROM BiContratosempleado b WHERE b.horActividad = :horActividad")})
public class BiContratosempleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codContrato")
    private Integer codContrato;
    @Column(name = "TipContrato")
    private Character tipContrato;
    @Column(name = "fecInicio")
    @Temporal(TemporalType.DATE)
    private Date fecInicio;
    @Column(name = "fecFin")
    @Temporal(TemporalType.DATE)
    private Date fecFin;
    @Column(name = "actUsuario")
    private Integer actUsuario;
    @Column(name = "estActividad")
    private Character estActividad;
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(mappedBy = "codContrato")
    private List<BiSueldoempleado> biSueldoempleados;
    @OneToMany(mappedBy = "codContratoSolicitante")
    private List<BeSolicitud> beSolicitudes;
    @JoinColumn(name = "codTercero", referencedColumnName = "codTercero")
    @ManyToOne
    private BiTercero codTercero;

    public BiContratosempleado() {
    }

    public BiContratosempleado(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public Integer getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(Integer codContrato) {
        this.codContrato = codContrato;
    }

    public Character getTipContrato() {
        return tipContrato;
    }

    public void setTipContrato(Character tipContrato) {
        this.tipContrato = tipContrato;
    }

    public Date getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Date fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Date getFecFin() {
        return fecFin;
    }

    public void setFecFin(Date fecFin) {
        this.fecFin = fecFin;
    }

    public Integer getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(Integer actUsuario) {
        this.actUsuario = actUsuario;
    }

    public Character getEstActividad() {
        return estActividad;
    }

    public void setEstActividad(Character estActividad) {
        this.estActividad = estActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<BiSueldoempleado> getBiSueldoempleados() {
        return biSueldoempleados;
    }

    public void setBiSueldoempleados(List<BiSueldoempleado> biSueldoempleados) {
        this.biSueldoempleados = biSueldoempleados;
    }

    public List<BeSolicitud> getBeSolicitudes() {
        return beSolicitudes;
    }

    public void setBeSolicitudes(List<BeSolicitud> beSolicitudes) {
        this.beSolicitudes = beSolicitudes;
    }

    public BiTercero getCodTercero() {
        return codTercero;
    }

    public void setCodTercero(BiTercero codTercero) {
        this.codTercero = codTercero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codContrato != null ? codContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BiContratosempleado)) {
            return false;
        }
        BiContratosempleado other = (BiContratosempleado) object;
        if ((this.codContrato == null && other.codContrato != null) || (this.codContrato != null && !this.codContrato.equals(other.codContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BiContratosempleado[ codContrato=" + codContrato + " ]";
    }
    
}
