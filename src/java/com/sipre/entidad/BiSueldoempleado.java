/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "bi_sueldoempleado")
@NamedQueries({
    @NamedQuery(name = "BiSueldoempleado.findAll", query = "SELECT b FROM BiSueldoempleado b")
    , @NamedQuery(name = "BiSueldoempleado.findByCodSueldo", query = "SELECT b FROM BiSueldoempleado b WHERE b.codSueldo = :codSueldo")
    , @NamedQuery(name = "BiSueldoempleado.findBySueldo", query = "SELECT b FROM BiSueldoempleado b WHERE b.sueldo = :sueldo")
    , @NamedQuery(name = "BiSueldoempleado.findByActUsuario", query = "SELECT b FROM BiSueldoempleado b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BiSueldoempleado.findByEstActividad", query = "SELECT b FROM BiSueldoempleado b WHERE b.estActividad = :estActividad")
    , @NamedQuery(name = "BiSueldoempleado.findByHorActividad", query = "SELECT b FROM BiSueldoempleado b WHERE b.horActividad = :horActividad")})
public class BiSueldoempleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codSueldo")
    private Integer codSueldo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Sueldo")
    private Double sueldo;
    @Column(name = "actUsuario")
    private Integer actUsuario;
    @Column(name = "estActividad")
    private Character estActividad;
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @JoinColumn(name = "codContrato", referencedColumnName = "codContrato")
    @ManyToOne
    private BiContratosempleado codContrato;

    public BiSueldoempleado() {
    }

    public BiSueldoempleado(Integer codSueldo) {
        this.codSueldo = codSueldo;
    }

    public Integer getCodSueldo() {
        return codSueldo;
    }

    public void setCodSueldo(Integer codSueldo) {
        this.codSueldo = codSueldo;
    }

    public Double getSueldo() {
        return sueldo;
    }

    public void setSueldo(Double sueldo) {
        this.sueldo = sueldo;
    }

    public Integer getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(Integer actUsuario) {
        this.actUsuario = actUsuario;
    }

    public Character getEstActividad() {
        return estActividad;
    }

    public void setEstActividad(Character estActividad) {
        this.estActividad = estActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public BiContratosempleado getCodContrato() {
        return codContrato;
    }

    public void setCodContrato(BiContratosempleado codContrato) {
        this.codContrato = codContrato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSueldo != null ? codSueldo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BiSueldoempleado)) {
            return false;
        }
        BiSueldoempleado other = (BiSueldoempleado) object;
        if ((this.codSueldo == null && other.codSueldo != null) || (this.codSueldo != null && !this.codSueldo.equals(other.codSueldo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BiSueldoempleado[ codSueldo=" + codSueldo + " ]";
    }
    
}
