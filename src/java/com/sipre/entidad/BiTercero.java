/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "bi_terceros")
@NamedQueries({
    @NamedQuery(name = "BiTercero.findAll", query = "SELECT b FROM BiTercero b")
    , @NamedQuery(name = "BiTercero.findByCodTercero", query = "SELECT b FROM BiTercero b WHERE b.codTercero = :codTercero")
    , @NamedQuery(name = "BiTercero.findByTipTercero", query = "SELECT b FROM BiTercero b WHERE b.tipTercero = :tipTercero")
    , @NamedQuery(name = "BiTercero.findByPriNombre", query = "SELECT b FROM BiTercero b WHERE b.priNombre = :priNombre")
    , @NamedQuery(name = "BiTercero.findBySegNombre", query = "SELECT b FROM BiTercero b WHERE b.segNombre = :segNombre")
    , @NamedQuery(name = "BiTercero.findByPriApellido", query = "SELECT b FROM BiTercero b WHERE b.priApellido = :priApellido")
    , @NamedQuery(name = "BiTercero.findBySegApellido", query = "SELECT b FROM BiTercero b WHERE b.segApellido = :segApellido")
    , @NamedQuery(name = "BiTercero.findByFecNacimiento", query = "SELECT b FROM BiTercero b WHERE b.fecNacimiento = :fecNacimiento")
    , @NamedQuery(name = "BiTercero.findByIndActividad", query = "SELECT b FROM BiTercero b WHERE b.indActividad = :indActividad")
    , @NamedQuery(name = "BiTercero.findByDirResidencia", query = "SELECT b FROM BiTercero b WHERE b.dirResidencia = :dirResidencia")
    , @NamedQuery(name = "BiTercero.findByTelResidencia", query = "SELECT b FROM BiTercero b WHERE b.telResidencia = :telResidencia")
    , @NamedQuery(name = "BiTercero.findByCorElectronico", query = "SELECT b FROM BiTercero b WHERE b.corElectronico = :corElectronico")
    , @NamedQuery(name = "BiTercero.findByClave", query = "SELECT b FROM BiTercero b WHERE b.clave = :clave")
    , @NamedQuery(name = "BiTercero.findByActUsuario", query = "SELECT b FROM BiTercero b WHERE b.actUsuario = :actUsuario")
    , @NamedQuery(name = "BiTercero.findByActEsta", query = "SELECT b FROM BiTercero b WHERE b.actEsta = :actEsta")
    , @NamedQuery(name = "BiTercero.findByActHora", query = "SELECT b FROM BiTercero b WHERE b.actHora = :actHora")})
public class BiTercero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTercero")
    private Integer codTercero;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipTercero")
    private Character tipTercero;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "priNombre")
    private String priNombre;
    @Size(max = 20)
    @Column(name = "segNombre")
    private String segNombre;
    @Size(max = 20)
    @Column(name = "priApellido")
    private String priApellido;
    @Size(max = 20)
    @Column(name = "segApellido")
    private String segApellido;
    @Column(name = "fecNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fecNacimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "indActividad")
    private Character indActividad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "dirResidencia")
    private String dirResidencia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "telResidencia")
    private int telResidencia;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "corElectronico")
    private String corElectronico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "clave")
    private String clave;
    @Size(max = 15)
    @Column(name = "actUsuario")
    private String actUsuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actEsta")
    private Character actEsta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "actHora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actHora;
    @JoinTable(name = "gn_rol_bi_tercero", joinColumns = {
        @JoinColumn(name = "CodTerceros", referencedColumnName = "codTercero")}, inverseJoinColumns = {
        @JoinColumn(name = "CodRol", referencedColumnName = "codRol")})
    @ManyToMany
    private List<GnRol> gnRoles;
    @ManyToMany(mappedBy = "biTerceros")
    private List<GnDocadjunto> gnDocadjuntos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codTomador")
    private List<BeSolicitud> beSolicitudes;
    @OneToMany(mappedBy = "codTercero")
    private List<BiContratosempleado> biContratosempleados;
    @JoinColumn(name = "codMunicipioResidencia", referencedColumnName = "codMunicipio")
    @ManyToOne(optional = false)
    private GnMunicipio codMunicipioResidencia;
    @JoinColumn(name = "munNacimiento", referencedColumnName = "codMunicipio")
    @ManyToOne
    private GnMunicipio munNacimiento;
    @JoinColumn(name = "tipIdentificacion", referencedColumnName = "codTipoIdentificacion")
    @ManyToOne(optional = false)
    private GnTipoidentificacion tipIdentificacion;
    @OneToMany(mappedBy = "codEmpleado")
    private List<BiTercero> biTerceros;
    @JoinColumn(name = "codEmpleado", referencedColumnName = "codTercero")
    @ManyToOne
    private BiTercero codEmpleado;
    @JoinColumn(name = "codTipoBeneficiario", referencedColumnName = "codTipoBeneficiarios")
    @ManyToOne
    private GnTipobeneficiario codTipoBeneficiario;
    @OneToMany(mappedBy = "codProveedor")
    private List<BiContratoproveedor> biContratoproveedores;

    public BiTercero() {
    }

    public BiTercero(Integer codTercero) {
        this.codTercero = codTercero;
    }

    public BiTercero(Integer codTercero, Character tipTercero, String priNombre, Character indActividad, String dirResidencia, int telResidencia, String corElectronico, String clave, Character actEsta, Date actHora) {
        this.codTercero = codTercero;
        this.tipTercero = tipTercero;
        this.priNombre = priNombre;
        this.indActividad = indActividad;
        this.dirResidencia = dirResidencia;
        this.telResidencia = telResidencia;
        this.corElectronico = corElectronico;
        this.clave = clave;
        this.actEsta = actEsta;
        this.actHora = actHora;
    }

    public Integer getCodTercero() {
        return codTercero;
    }

    public void setCodTercero(Integer codTercero) {
        this.codTercero = codTercero;
    }

    public Character getTipTercero() {
        return tipTercero;
    }

    public void setTipTercero(Character tipTercero) {
        this.tipTercero = tipTercero;
    }

    public String getPriNombre() {
        return priNombre;
    }

    public void setPriNombre(String priNombre) {
        this.priNombre = priNombre;
    }

    public String getSegNombre() {
        return segNombre;
    }

    public void setSegNombre(String segNombre) {
        this.segNombre = segNombre;
    }

    public String getPriApellido() {
        return priApellido;
    }

    public void setPriApellido(String priApellido) {
        this.priApellido = priApellido;
    }

    public String getSegApellido() {
        return segApellido;
    }

    public void setSegApellido(String segApellido) {
        this.segApellido = segApellido;
    }

    public Date getFecNacimiento() {
        return fecNacimiento;
    }

    public void setFecNacimiento(Date fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    public Character getIndActividad() {
        return indActividad;
    }

    public void setIndActividad(Character indActividad) {
        this.indActividad = indActividad;
    }

    public String getDirResidencia() {
        return dirResidencia;
    }

    public void setDirResidencia(String dirResidencia) {
        this.dirResidencia = dirResidencia;
    }

    public int getTelResidencia() {
        return telResidencia;
    }

    public void setTelResidencia(int telResidencia) {
        this.telResidencia = telResidencia;
    }

    public String getCorElectronico() {
        return corElectronico;
    }

    public void setCorElectronico(String corElectronico) {
        this.corElectronico = corElectronico;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getActUsuario() {
        return actUsuario;
    }

    public void setActUsuario(String actUsuario) {
        this.actUsuario = actUsuario;
    }

    public Character getActEsta() {
        return actEsta;
    }

    public void setActEsta(Character actEsta) {
        this.actEsta = actEsta;
    }

    public Date getActHora() {
        return actHora;
    }

    public void setActHora(Date actHora) {
        this.actHora = actHora;
    }

    public List<GnRol> getGnRoles() {
        return gnRoles;
    }

    public void setGnRoles(List<GnRol> gnRoles) {
        this.gnRoles = gnRoles;
    }

    public List<GnDocadjunto> getGnDocadjuntos() {
        return gnDocadjuntos;
    }

    public void setGnDocadjuntos(List<GnDocadjunto> gnDocadjuntos) {
        this.gnDocadjuntos = gnDocadjuntos;
    }

    public List<BeSolicitud> getBeSolicitudes() {
        return beSolicitudes;
    }

    public void setBeSolicitudes(List<BeSolicitud> beSolicitudes) {
        this.beSolicitudes = beSolicitudes;
    }

    public List<BiContratosempleado> getBiContratosempleados() {
        return biContratosempleados;
    }

    public void setBiContratosempleados(List<BiContratosempleado> biContratosempleados) {
        this.biContratosempleados = biContratosempleados;
    }

    public GnMunicipio getCodMunicipioResidencia() {
        return codMunicipioResidencia;
    }

    public void setCodMunicipioResidencia(GnMunicipio codMunicipioResidencia) {
        this.codMunicipioResidencia = codMunicipioResidencia;
    }

    public GnMunicipio getMunNacimiento() {
        return munNacimiento;
    }

    public void setMunNacimiento(GnMunicipio munNacimiento) {
        this.munNacimiento = munNacimiento;
    }

    public GnTipoidentificacion getTipIdentificacion() {
        return tipIdentificacion;
    }

    public void setTipIdentificacion(GnTipoidentificacion tipIdentificacion) {
        this.tipIdentificacion = tipIdentificacion;
    }

    public List<BiTercero> getBiTerceros() {
        return biTerceros;
    }

    public void setBiTerceros(List<BiTercero> biTerceros) {
        this.biTerceros = biTerceros;
    }

    public BiTercero getCodEmpleado() {
        return codEmpleado;
    }

    public void setCodEmpleado(BiTercero codEmpleado) {
        this.codEmpleado = codEmpleado;
    }

    public GnTipobeneficiario getCodTipoBeneficiario() {
        return codTipoBeneficiario;
    }

    public void setCodTipoBeneficiario(GnTipobeneficiario codTipoBeneficiario) {
        this.codTipoBeneficiario = codTipoBeneficiario;
    }

    public List<BiContratoproveedor> getBiContratoproveedores() {
        return biContratoproveedores;
    }

    public void setBiContratoproveedores(List<BiContratoproveedor> biContratoproveedores) {
        this.biContratoproveedores = biContratoproveedores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTercero != null ? codTercero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BiTercero)) {
            return false;
        }
        BiTercero other = (BiTercero) object;
        if ((this.codTercero == null && other.codTercero != null) || (this.codTercero != null && !this.codTercero.equals(other.codTercero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.BiTercero[ codTercero=" + codTercero + " ]";
    }
    
}
