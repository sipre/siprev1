/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_auditoria")
@NamedQueries({
    @NamedQuery(name = "GnAuditoria.findAll", query = "SELECT g FROM GnAuditoria g")
    , @NamedQuery(name = "GnAuditoria.findByCodAuditoria", query = "SELECT g FROM GnAuditoria g WHERE g.codAuditoria = :codAuditoria")
    , @NamedQuery(name = "GnAuditoria.findByFecha", query = "SELECT g FROM GnAuditoria g WHERE g.fecha = :fecha")
    , @NamedQuery(name = "GnAuditoria.findByTipoModificacion", query = "SELECT g FROM GnAuditoria g WHERE g.tipoModificacion = :tipoModificacion")
    , @NamedQuery(name = "GnAuditoria.findByUsuario", query = "SELECT g FROM GnAuditoria g WHERE g.usuario = :usuario")
    , @NamedQuery(name = "GnAuditoria.findByTabla", query = "SELECT g FROM GnAuditoria g WHERE g.tabla = :tabla")
    , @NamedQuery(name = "GnAuditoria.findByTipTercero", query = "SELECT g FROM GnAuditoria g WHERE g.tipTercero = :tipTercero")
    , @NamedQuery(name = "GnAuditoria.findByCodTercero", query = "SELECT g FROM GnAuditoria g WHERE g.codTercero = :codTercero")
    , @NamedQuery(name = "GnAuditoria.findByTipIdentificacion", query = "SELECT g FROM GnAuditoria g WHERE g.tipIdentificacion = :tipIdentificacion")})
public class GnAuditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codAuditoria")
    private Integer codAuditoria;
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Size(max = 10)
    @Column(name = "tipoModificacion")
    private String tipoModificacion;
    @Size(max = 50)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 50)
    @Column(name = "tabla")
    private String tabla;
    @Column(name = "tipTercero")
    private Character tipTercero;
    @Column(name = "codTercero")
    private Integer codTercero;
    @Column(name = "TipIdentificacion")
    private Character tipIdentificacion;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DesTercero")
    private String desTercero;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;

    public GnAuditoria() {
    }

    public GnAuditoria(Integer codAuditoria) {
        this.codAuditoria = codAuditoria;
    }

    public Integer getCodAuditoria() {
        return codAuditoria;
    }

    public void setCodAuditoria(Integer codAuditoria) {
        this.codAuditoria = codAuditoria;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipoModificacion() {
        return tipoModificacion;
    }

    public void setTipoModificacion(String tipoModificacion) {
        this.tipoModificacion = tipoModificacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    public Character getTipTercero() {
        return tipTercero;
    }

    public void setTipTercero(Character tipTercero) {
        this.tipTercero = tipTercero;
    }

    public Integer getCodTercero() {
        return codTercero;
    }

    public void setCodTercero(Integer codTercero) {
        this.codTercero = codTercero;
    }

    public Character getTipIdentificacion() {
        return tipIdentificacion;
    }

    public void setTipIdentificacion(Character tipIdentificacion) {
        this.tipIdentificacion = tipIdentificacion;
    }

    public String getDesTercero() {
        return desTercero;
    }

    public void setDesTercero(String desTercero) {
        this.desTercero = desTercero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codAuditoria != null ? codAuditoria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnAuditoria)) {
            return false;
        }
        GnAuditoria other = (GnAuditoria) object;
        if ((this.codAuditoria == null && other.codAuditoria != null) || (this.codAuditoria != null && !this.codAuditoria.equals(other.codAuditoria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnAuditoria[ codAuditoria=" + codAuditoria + " ]";
    }
    
}
