/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_docadjuntos")
@NamedQueries({
    @NamedQuery(name = "GnDocadjunto.findAll", query = "SELECT g FROM GnDocadjunto g")
    , @NamedQuery(name = "GnDocadjunto.findByCodDocumento", query = "SELECT g FROM GnDocadjunto g WHERE g.codDocumento = :codDocumento")
    , @NamedQuery(name = "GnDocadjunto.findByRutArchivo", query = "SELECT g FROM GnDocadjunto g WHERE g.rutArchivo = :rutArchivo")
    , @NamedQuery(name = "GnDocadjunto.findByUsuActividad", query = "SELECT g FROM GnDocadjunto g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnDocadjunto.findByTipActividad", query = "SELECT g FROM GnDocadjunto g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnDocadjunto.findByHorActividad", query = "SELECT g FROM GnDocadjunto g WHERE g.horActividad = :horActividad")})
public class GnDocadjunto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codDocumento")
    private Integer codDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rutArchivo")
    private int rutArchivo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "usuActividad")
    private String usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @JoinTable(name = "gn_docadjunto_bi_terceros", joinColumns = {
        @JoinColumn(name = "CodDocumento", referencedColumnName = "codDocumento")}, inverseJoinColumns = {
        @JoinColumn(name = "codTercero", referencedColumnName = "codTercero")})
    @ManyToMany
    private List<BiTercero> biTerceros;
    @JoinColumn(name = "tipDocumento", referencedColumnName = "codTipoDocumentos")
    @ManyToOne(optional = false)
    private GnTipodocumento tipDocumento;

    public GnDocadjunto() {
    }

    public GnDocadjunto(Integer codDocumento) {
        this.codDocumento = codDocumento;
    }

    public GnDocadjunto(Integer codDocumento, int rutArchivo, String usuActividad, Character tipActividad, Date horActividad) {
        this.codDocumento = codDocumento;
        this.rutArchivo = rutArchivo;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodDocumento() {
        return codDocumento;
    }

    public void setCodDocumento(Integer codDocumento) {
        this.codDocumento = codDocumento;
    }

    public int getRutArchivo() {
        return rutArchivo;
    }

    public void setRutArchivo(int rutArchivo) {
        this.rutArchivo = rutArchivo;
    }

    public String getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(String usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<BiTercero> getBiTerceros() {
        return biTerceros;
    }

    public void setBiTerceros(List<BiTercero> biTerceros) {
        this.biTerceros = biTerceros;
    }

    public GnTipodocumento getTipDocumento() {
        return tipDocumento;
    }

    public void setTipDocumento(GnTipodocumento tipDocumento) {
        this.tipDocumento = tipDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codDocumento != null ? codDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnDocadjunto)) {
            return false;
        }
        GnDocadjunto other = (GnDocadjunto) object;
        if ((this.codDocumento == null && other.codDocumento != null) || (this.codDocumento != null && !this.codDocumento.equals(other.codDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnDocadjunto[ codDocumento=" + codDocumento + " ]";
    }
    
}
