/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_municipios")
@NamedQueries({
    @NamedQuery(name = "GnMunicipio.findAll", query = "SELECT g FROM GnMunicipio g")
    , @NamedQuery(name = "GnMunicipio.findByCodMunicipio", query = "SELECT g FROM GnMunicipio g WHERE g.codMunicipio = :codMunicipio")
    , @NamedQuery(name = "GnMunicipio.findByCodSuip", query = "SELECT g FROM GnMunicipio g WHERE g.codSuip = :codSuip")
    , @NamedQuery(name = "GnMunicipio.findByNomMunicipio", query = "SELECT g FROM GnMunicipio g WHERE g.nomMunicipio = :nomMunicipio")
    , @NamedQuery(name = "GnMunicipio.findByUsuActividad", query = "SELECT g FROM GnMunicipio g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnMunicipio.findByTipActividad", query = "SELECT g FROM GnMunicipio g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnMunicipio.findByHorActividad", query = "SELECT g FROM GnMunicipio g WHERE g.horActividad = :horActividad")})
public class GnMunicipio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codMunicipio")
    private Integer codMunicipio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "codSuip")
    private int codSuip;
    @Size(max = 30)
    @Column(name = "nomMunicipio")
    private String nomMunicipio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "usuActividad")
    private String usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @JoinColumn(name = "codDepartamento", referencedColumnName = "codDepartamento")
    @ManyToOne(optional = false)
    private GnDepartamento codDepartamento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codMunicipioResidencia")
    private List<BiTercero> biTerceroMunicipioResidencia;
    @OneToMany(mappedBy = "munNacimiento")
    private List<BiTercero> biTerceroMunNacimineto;

    public GnMunicipio() {
    }

    public GnMunicipio(Integer codMunicipio) {
        this.codMunicipio = codMunicipio;
    }

    public GnMunicipio(Integer codMunicipio, int codSuip, String usuActividad, Character tipActividad, Date horActividad) {
        this.codMunicipio = codMunicipio;
        this.codSuip = codSuip;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodMunicipio() {
        return codMunicipio;
    }

    public void setCodMunicipio(Integer codMunicipio) {
        this.codMunicipio = codMunicipio;
    }

    public int getCodSuip() {
        return codSuip;
    }

    public void setCodSuip(int codSuip) {
        this.codSuip = codSuip;
    }

    public String getNomMunicipio() {
        return nomMunicipio;
    }

    public void setNomMunicipio(String nomMunicipio) {
        this.nomMunicipio = nomMunicipio;
    }

    public String getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(String usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public GnDepartamento getCodDepartamento() {
        return codDepartamento;
    }

    public void setCodDepartamento(GnDepartamento codDepartamento) {
        this.codDepartamento = codDepartamento;
    }

    public List<BiTercero> getBiTerceroList() {
        return biTerceroMunicipioResidencia;
    }

    public void setBiTerceroList(List<BiTercero> biTerceroMunicipioResidencia) {
        this.biTerceroMunicipioResidencia = biTerceroMunicipioResidencia;
    }

    public List<BiTercero> getBiTerceroList1() {
        return biTerceroMunNacimineto;
    }

    public void setBiTerceroList1(List<BiTercero> biTerceroMunNacimineto) {
        this.biTerceroMunNacimineto = biTerceroMunNacimineto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codMunicipio != null ? codMunicipio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnMunicipio)) {
            return false;
        }
        GnMunicipio other = (GnMunicipio) object;
        if ((this.codMunicipio == null && other.codMunicipio != null) || (this.codMunicipio != null && !this.codMunicipio.equals(other.codMunicipio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnMunicipio[ codMunicipio=" + codMunicipio + " ]";
    }
    
}
