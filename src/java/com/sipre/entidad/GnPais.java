/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_pais")
@NamedQueries({
    @NamedQuery(name = "GnPais.findAll", query = "SELECT g FROM GnPais g")
    , @NamedQuery(name = "GnPais.findByCodPais", query = "SELECT g FROM GnPais g WHERE g.codPais = :codPais")
    , @NamedQuery(name = "GnPais.findByNomPais", query = "SELECT g FROM GnPais g WHERE g.nomPais = :nomPais")
    , @NamedQuery(name = "GnPais.findByUsuActividad", query = "SELECT g FROM GnPais g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnPais.findByTipActividad", query = "SELECT g FROM GnPais g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnPais.findByHorActividad", query = "SELECT g FROM GnPais g WHERE g.horActividad = :horActividad")})
public class GnPais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codPais")
    private Integer codPais;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nomPais")
    private String nomPais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuActividad")
    private int usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codPais")
    private List<GnDepartamento> gnDepartamentos;

    public GnPais() {
    }

    public GnPais(Integer codPais) {
        this.codPais = codPais;
    }

    public GnPais(Integer codPais, String nomPais, int usuActividad, Character tipActividad, Date horActividad) {
        this.codPais = codPais;
        this.nomPais = nomPais;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodPais() {
        return codPais;
    }

    public void setCodPais(Integer codPais) {
        this.codPais = codPais;
    }

    public String getNomPais() {
        return nomPais;
    }

    public void setNomPais(String nomPais) {
        this.nomPais = nomPais;
    }

    public int getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(int usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<GnDepartamento> getGnDepartamentos() {
        return gnDepartamentos;
    }

    public void setGnDepartamentos(List<GnDepartamento> gnDepartamentos) {
        this.gnDepartamentos = gnDepartamentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPais != null ? codPais.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnPais)) {
            return false;
        }
        GnPais other = (GnPais) object;
        if ((this.codPais == null && other.codPais != null) || (this.codPais != null && !this.codPais.equals(other.codPais))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnPais[ codPais=" + codPais + " ]";
    }
    
}
