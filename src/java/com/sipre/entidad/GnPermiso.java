/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_permisos")
@NamedQueries({
    @NamedQuery(name = "GnPermiso.findAll", query = "SELECT g FROM GnPermiso g")
    , @NamedQuery(name = "GnPermiso.findByIdPermiso", query = "SELECT g FROM GnPermiso g WHERE g.idPermiso = :idPermiso")
    , @NamedQuery(name = "GnPermiso.findByNombrePermiso", query = "SELECT g FROM GnPermiso g WHERE g.nombrePermiso = :nombrePermiso")
    , @NamedQuery(name = "GnPermiso.findByUrl", query = "SELECT g FROM GnPermiso g WHERE g.url = :url")
    , @NamedQuery(name = "GnPermiso.findByUsuActividad", query = "SELECT g FROM GnPermiso g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnPermiso.findByTipActividad", query = "SELECT g FROM GnPermiso g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnPermiso.findByHorActividad", query = "SELECT g FROM GnPermiso g WHERE g.horActividad = :horActividad")})
public class GnPermiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IdPermiso")
    private Integer idPermiso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "nombrePermiso")
    private String nombrePermiso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "url")
    private String url;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuActividad")
    private int usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    
    @ManyToMany(mappedBy = "gnPermisos")
    private List<GnRol> gnRoles;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permisoPadre")
    private List<GnPermiso> subPermisos;
    
    @JoinColumn(name = "permisoPadre", referencedColumnName = "IdPermiso")
    @ManyToOne(optional = false)
    private GnPermiso permisoPadre;

    public GnPermiso() {
    }

    public GnPermiso(Integer idPermiso) {
        this.idPermiso = idPermiso;
    }

    public GnPermiso(Integer idPermiso, String nombrePermiso, String url, int usuActividad, Character tipActividad, Date horActividad) {
        this.idPermiso = idPermiso;
        this.nombrePermiso = nombrePermiso;
        this.url = url;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Integer idPermiso) {
        this.idPermiso = idPermiso;
    }

    public String getNombrePermiso() {
        return nombrePermiso;
    }

    public void setNombrePermiso(String nombrePermiso) {
        this.nombrePermiso = nombrePermiso;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(int usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<GnRol> getGnRoles() {
        return gnRoles;
    }

    public void setGnRoles(List<GnRol> gnRoles) {
        this.gnRoles = gnRoles;
    }

    public List<GnPermiso> getSubPermisos() {
        return subPermisos;
    }

    public void setSubPermisos(List<GnPermiso> subPermisos) {
        this.subPermisos = subPermisos;
    }

    public GnPermiso getPermisoPadre() {
        return permisoPadre;
    }

    public void setPermisoPadre(GnPermiso permisoPadre) {
        this.permisoPadre = permisoPadre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPermiso != null ? idPermiso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnPermiso)) {
            return false;
        }
        GnPermiso other = (GnPermiso) object;
        if ((this.idPermiso == null && other.idPermiso != null) || (this.idPermiso != null && !this.idPermiso.equals(other.idPermiso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnPermiso[ idPermiso=" + idPermiso + " ]";
    }
    
}
