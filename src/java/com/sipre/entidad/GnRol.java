/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_roles")
@NamedQueries({
    @NamedQuery(name = "GnRol.findAll", query = "SELECT g FROM GnRol g")
    , @NamedQuery(name = "GnRol.findByCodRol", query = "SELECT g FROM GnRol g WHERE g.codRol = :codRol")
    , @NamedQuery(name = "GnRol.findByNomRol", query = "SELECT g FROM GnRol g WHERE g.nomRol = :nomRol")
    , @NamedQuery(name = "GnRol.findByTipRol", query = "SELECT g FROM GnRol g WHERE g.tipRol = :tipRol")
    , @NamedQuery(name = "GnRol.findByUsuActividad", query = "SELECT g FROM GnRol g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnRol.findByTipActividad", query = "SELECT g FROM GnRol g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnRol.findByHorActividad", query = "SELECT g FROM GnRol g WHERE g.horActividad = :horActividad")})
public class GnRol implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codRol")
    private Integer codRol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nomRol")
    private String nomRol;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "TipRol")
    private String tipRol;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuActividad")
    private int usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @JoinTable(name = "gn_permisos_gn_roles", joinColumns = {
        @JoinColumn(name = "idRol", referencedColumnName = "codRol")}, inverseJoinColumns = {
        @JoinColumn(name = "idPermiso", referencedColumnName = "idPermiso")})
    @ManyToMany
    private List<GnPermiso> gnPermisos;
    
     @JoinTable(name = "gn_rol_bi_tercero", joinColumns = {
        @JoinColumn(name = "CodRol", referencedColumnName = "codRol")}, inverseJoinColumns = {
        @JoinColumn(name = "CodTerceros", referencedColumnName = "codTercero")})
    @ManyToMany(mappedBy = "gnRoles")
    private List<BiTercero> biTerceros;

    public GnRol() {
    }

    public GnRol(Integer codRol) {
        this.codRol = codRol;
    }

    public GnRol(Integer codRol, String nomRol, String tipRol, int usuActividad, Character tipActividad, Date horActividad) {
        this.codRol = codRol;
        this.nomRol = nomRol;
        this.tipRol = tipRol;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodRol() {
        return codRol;
    }

    public void setCodRol(Integer codRol) {
        this.codRol = codRol;
    }

    public String getNomRol() {
        return nomRol;
    }

    public void setNomRol(String nomRol) {
        this.nomRol = nomRol;
    }

    public String getTipRol() {
        return tipRol;
    }

    public void setTipRol(String tipRol) {
        this.tipRol = tipRol;
    }

    public int getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(int usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<GnPermiso> getGnPermisos() {
        return gnPermisos;
    }

    public void setGnPermisos(List<GnPermiso> gnPermisos) {
        this.gnPermisos = gnPermisos;
    }

    public List<BiTercero> getBiTerceros() {
        return biTerceros;
    }

    public void setBiTerceros(List<BiTercero> biTerceros) {
        this.biTerceros = biTerceros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codRol != null ? codRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnRol)) {
            return false;
        }
        GnRol other = (GnRol) object;
        if ((this.codRol == null && other.codRol != null) || (this.codRol != null && !this.codRol.equals(other.codRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnRol[ codRol=" + codRol + " ]";
    }
    
}
