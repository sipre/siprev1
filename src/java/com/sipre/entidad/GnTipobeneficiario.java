/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_tipobeneficiarios")
@NamedQueries({
    @NamedQuery(name = "GnTipobeneficiario.findAll", query = "SELECT g FROM GnTipobeneficiario g")
    , @NamedQuery(name = "GnTipobeneficiario.findByCodTipoBeneficiarios", query = "SELECT g FROM GnTipobeneficiario g WHERE g.codTipoBeneficiarios = :codTipoBeneficiarios")
    , @NamedQuery(name = "GnTipobeneficiario.findByNomTipoBeneficiario", query = "SELECT g FROM GnTipobeneficiario g WHERE g.nomTipoBeneficiario = :nomTipoBeneficiario")
    , @NamedQuery(name = "GnTipobeneficiario.findByCantidadBemeficiario", query = "SELECT g FROM GnTipobeneficiario g WHERE g.cantidadBemeficiario = :cantidadBemeficiario")
    , @NamedQuery(name = "GnTipobeneficiario.findByUsuActividad", query = "SELECT g FROM GnTipobeneficiario g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnTipobeneficiario.findByTipActividad", query = "SELECT g FROM GnTipobeneficiario g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnTipobeneficiario.findByHorActividad", query = "SELECT g FROM GnTipobeneficiario g WHERE g.horActividad = :horActividad")})
public class GnTipobeneficiario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTipoBeneficiarios")
    private Integer codTipoBeneficiarios;
    @Size(max = 50)
    @Column(name = "nomTipoBeneficiario")
    private String nomTipoBeneficiario;
    @Column(name = "cantidadBemeficiario")
    private Integer cantidadBemeficiario;
    @Column(name = "usuActividad")
    private Integer usuActividad;
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(mappedBy = "codTipoBeneficiario")
    private List<BiTercero> biTerceros;

    public GnTipobeneficiario() {
    }

    public GnTipobeneficiario(Integer codTipoBeneficiarios) {
        this.codTipoBeneficiarios = codTipoBeneficiarios;
    }

    public Integer getCodTipoBeneficiarios() {
        return codTipoBeneficiarios;
    }

    public void setCodTipoBeneficiarios(Integer codTipoBeneficiarios) {
        this.codTipoBeneficiarios = codTipoBeneficiarios;
    }

    public String getNomTipoBeneficiario() {
        return nomTipoBeneficiario;
    }

    public void setNomTipoBeneficiario(String nomTipoBeneficiario) {
        this.nomTipoBeneficiario = nomTipoBeneficiario;
    }

    public Integer getCantidadBemeficiario() {
        return cantidadBemeficiario;
    }

    public void setCantidadBemeficiario(Integer cantidadBemeficiario) {
        this.cantidadBemeficiario = cantidadBemeficiario;
    }

    public Integer getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(Integer usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<BiTercero> getBiTerceros() {
        return biTerceros;
    }

    public void setBiTerceros(List<BiTercero> biTerceros) {
        this.biTerceros = biTerceros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoBeneficiarios != null ? codTipoBeneficiarios.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnTipobeneficiario)) {
            return false;
        }
        GnTipobeneficiario other = (GnTipobeneficiario) object;
        if ((this.codTipoBeneficiarios == null && other.codTipoBeneficiarios != null) || (this.codTipoBeneficiarios != null && !this.codTipoBeneficiarios.equals(other.codTipoBeneficiarios))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnTipobeneficiario[ codTipoBeneficiarios=" + codTipoBeneficiarios + " ]";
    }
    
}
