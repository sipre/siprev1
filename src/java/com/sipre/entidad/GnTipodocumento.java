/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_tipodocumentos")
@NamedQueries({
    @NamedQuery(name = "GnTipodocumento.findAll", query = "SELECT g FROM GnTipodocumento g")
    , @NamedQuery(name = "GnTipodocumento.findByCodTipoDocumentos", query = "SELECT g FROM GnTipodocumento g WHERE g.codTipoDocumentos = :codTipoDocumentos")
    , @NamedQuery(name = "GnTipodocumento.findByNomDocumento", query = "SELECT g FROM GnTipodocumento g WHERE g.nomDocumento = :nomDocumento")
    , @NamedQuery(name = "GnTipodocumento.findByUsuActividad", query = "SELECT g FROM GnTipodocumento g WHERE g.usuActividad = :usuActividad")
    , @NamedQuery(name = "GnTipodocumento.findByTipActividad", query = "SELECT g FROM GnTipodocumento g WHERE g.tipActividad = :tipActividad")
    , @NamedQuery(name = "GnTipodocumento.findByHorActividad", query = "SELECT g FROM GnTipodocumento g WHERE g.horActividad = :horActividad")})
public class GnTipodocumento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTipoDocumentos")
    private Integer codTipoDocumentos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nomDocumento")
    private int nomDocumento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuActividad")
    private int usuActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "horActividad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActividad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipDocumento")
    private List<GnDocadjunto> gnDocadjuntos;

    public GnTipodocumento() {
    }

    public GnTipodocumento(Integer codTipoDocumentos) {
        this.codTipoDocumentos = codTipoDocumentos;
    }

    public GnTipodocumento(Integer codTipoDocumentos, int nomDocumento, int usuActividad, Character tipActividad, Date horActividad) {
        this.codTipoDocumentos = codTipoDocumentos;
        this.nomDocumento = nomDocumento;
        this.usuActividad = usuActividad;
        this.tipActividad = tipActividad;
        this.horActividad = horActividad;
    }

    public Integer getCodTipoDocumentos() {
        return codTipoDocumentos;
    }

    public void setCodTipoDocumentos(Integer codTipoDocumentos) {
        this.codTipoDocumentos = codTipoDocumentos;
    }

    public int getNomDocumento() {
        return nomDocumento;
    }

    public void setNomDocumento(int nomDocumento) {
        this.nomDocumento = nomDocumento;
    }

    public int getUsuActividad() {
        return usuActividad;
    }

    public void setUsuActividad(int usuActividad) {
        this.usuActividad = usuActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActividad() {
        return horActividad;
    }

    public void setHorActividad(Date horActividad) {
        this.horActividad = horActividad;
    }

    public List<GnDocadjunto> getGnDocadjuntos() {
        return gnDocadjuntos;
    }

    public void setGnDocadjuntos(List<GnDocadjunto> gnDocadjuntos) {
        this.gnDocadjuntos = gnDocadjuntos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoDocumentos != null ? codTipoDocumentos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnTipodocumento)) {
            return false;
        }
        GnTipodocumento other = (GnTipodocumento) object;
        if ((this.codTipoDocumentos == null && other.codTipoDocumentos != null) || (this.codTipoDocumentos != null && !this.codTipoDocumentos.equals(other.codTipoDocumentos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnTipodocumento[ codTipoDocumentos=" + codTipoDocumentos + " ]";
    }
    
}
