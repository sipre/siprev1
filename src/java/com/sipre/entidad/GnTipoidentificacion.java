/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "gn_tipoidentificaciones")
@NamedQueries({
    @NamedQuery(name = "GnTipoidentificacion.findAll", query = "SELECT g FROM GnTipoidentificacion g")
    , @NamedQuery(name = "GnTipoidentificacion.findByCodTipoIdentificacion", query = "SELECT g FROM GnTipoidentificacion g WHERE g.codTipoIdentificacion = :codTipoIdentificacion")
    , @NamedQuery(name = "GnTipoidentificacion.findByNomTipoIdentificacion", query = "SELECT g FROM GnTipoidentificacion g WHERE g.nomTipoIdentificacion = :nomTipoIdentificacion")})
public class GnTipoidentificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codTipoIdentificacion")
    private Integer codTipoIdentificacion;
    @Size(max = 20)
    @Column(name = "nomTipoIdentificacion")
    private String nomTipoIdentificacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tipIdentificacion")
    private List<BiTercero> biTerceros;

    public GnTipoidentificacion() {
    }

    public GnTipoidentificacion(Integer codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public Integer getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(Integer codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public String getNomTipoIdentificacion() {
        return nomTipoIdentificacion;
    }

    public void setNomTipoIdentificacion(String nomTipoIdentificacion) {
        this.nomTipoIdentificacion = nomTipoIdentificacion;
    }

    public List<BiTercero> getBiTerceros() {
        return biTerceros;
    }

    public void setBiTerceros(List<BiTercero> biTerceros) {
        this.biTerceros = biTerceros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codTipoIdentificacion != null ? codTipoIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GnTipoidentificacion)) {
            return false;
        }
        GnTipoidentificacion other = (GnTipoidentificacion) object;
        if ((this.codTipoIdentificacion == null && other.codTipoIdentificacion != null) || (this.codTipoIdentificacion != null && !this.codTipoIdentificacion.equals(other.codTipoIdentificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.GnTipoidentificacion[ codTipoIdentificacion=" + codTipoIdentificacion + " ]";
    }
    
}
