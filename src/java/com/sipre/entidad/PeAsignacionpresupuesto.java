/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "pe_asignacionpresupuestos")
@NamedQueries({
    @NamedQuery(name = "PeAsignacionpresupuesto.findAll", query = "SELECT p FROM PeAsignacionpresupuesto p")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByCodPresupuesto", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.codPresupuesto = :codPresupuesto")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByCodTipoBeneficio", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.codTipoBeneficio = :codTipoBeneficio")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByAnoPresupuesto", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.anoPresupuesto = :anoPresupuesto")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByValInicial", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.valInicial = :valInicial")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByValEjecutado", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.valEjecutado = :valEjecutado")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByValPendiente", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.valPendiente = :valPendiente")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByUsoActividad", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.usoActividad = :usoActividad")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByTipActividad", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.tipActividad = :tipActividad")
    , @NamedQuery(name = "PeAsignacionpresupuesto.findByHorActivdad", query = "SELECT p FROM PeAsignacionpresupuesto p WHERE p.horActivdad = :horActivdad")})
public class PeAsignacionpresupuesto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codPresupuesto")
    private Integer codPresupuesto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "codTipoBeneficio")
    private int codTipoBeneficio;
    @Column(name = "anoPresupuesto")
    private Integer anoPresupuesto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valInicial")
    private Double valInicial;
    @Column(name = "valEjecutado")
    private Double valEjecutado;
    @Column(name = "valPendiente")
    private Double valPendiente;
    @Column(name = "usoActividad")
    private Integer usoActividad;
    @Column(name = "tipActividad")
    private Character tipActividad;
    @Column(name = "horActivdad")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horActivdad;

    public PeAsignacionpresupuesto() {
    }

    public PeAsignacionpresupuesto(Integer codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }

    public PeAsignacionpresupuesto(Integer codPresupuesto, int codTipoBeneficio) {
        this.codPresupuesto = codPresupuesto;
        this.codTipoBeneficio = codTipoBeneficio;
    }

    public Integer getCodPresupuesto() {
        return codPresupuesto;
    }

    public void setCodPresupuesto(Integer codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }

    public int getCodTipoBeneficio() {
        return codTipoBeneficio;
    }

    public void setCodTipoBeneficio(int codTipoBeneficio) {
        this.codTipoBeneficio = codTipoBeneficio;
    }

    public Integer getAnoPresupuesto() {
        return anoPresupuesto;
    }

    public void setAnoPresupuesto(Integer anoPresupuesto) {
        this.anoPresupuesto = anoPresupuesto;
    }

    public Double getValInicial() {
        return valInicial;
    }

    public void setValInicial(Double valInicial) {
        this.valInicial = valInicial;
    }

    public Double getValEjecutado() {
        return valEjecutado;
    }

    public void setValEjecutado(Double valEjecutado) {
        this.valEjecutado = valEjecutado;
    }

    public Double getValPendiente() {
        return valPendiente;
    }

    public void setValPendiente(Double valPendiente) {
        this.valPendiente = valPendiente;
    }

    public Integer getUsoActividad() {
        return usoActividad;
    }

    public void setUsoActividad(Integer usoActividad) {
        this.usoActividad = usoActividad;
    }

    public Character getTipActividad() {
        return tipActividad;
    }

    public void setTipActividad(Character tipActividad) {
        this.tipActividad = tipActividad;
    }

    public Date getHorActivdad() {
        return horActivdad;
    }

    public void setHorActivdad(Date horActivdad) {
        this.horActivdad = horActivdad;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codPresupuesto != null ? codPresupuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeAsignacionpresupuesto)) {
            return false;
        }
        PeAsignacionpresupuesto other = (PeAsignacionpresupuesto) object;
        if ((this.codPresupuesto == null && other.codPresupuesto != null) || (this.codPresupuesto != null && !this.codPresupuesto.equals(other.codPresupuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.PeAsignacionpresupuesto[ codPresupuesto=" + codPresupuesto + " ]";
    }
    
}
