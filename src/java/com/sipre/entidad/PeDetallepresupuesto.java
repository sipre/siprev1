/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author alejo
 */
@Entity
@Table(name = "pe_detallepresupuesto")
@NamedQueries({
    @NamedQuery(name = "PeDetallepresupuesto.findAll", query = "SELECT p FROM PeDetallepresupuesto p")
    , @NamedQuery(name = "PeDetallepresupuesto.findByCodDetallePresupuesto", query = "SELECT p FROM PeDetallepresupuesto p WHERE p.codDetallePresupuesto = :codDetallePresupuesto")
    , @NamedQuery(name = "PeDetallepresupuesto.findByCodPresupuesto", query = "SELECT p FROM PeDetallepresupuesto p WHERE p.codPresupuesto = :codPresupuesto")})
public class PeDetallepresupuesto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codDetallePresupuesto")
    private Integer codDetallePresupuesto;
    @Column(name = "codPresupuesto")
    private Integer codPresupuesto;
    @JoinColumn(name = "codSolicitud", referencedColumnName = "codSolicitud")
    @ManyToOne
    private BeSolicitud codSolicitud;

    public PeDetallepresupuesto() {
    }

    public PeDetallepresupuesto(Integer codDetallePresupuesto) {
        this.codDetallePresupuesto = codDetallePresupuesto;
    }

    public Integer getCodDetallePresupuesto() {
        return codDetallePresupuesto;
    }

    public void setCodDetallePresupuesto(Integer codDetallePresupuesto) {
        this.codDetallePresupuesto = codDetallePresupuesto;
    }

    public Integer getCodPresupuesto() {
        return codPresupuesto;
    }

    public void setCodPresupuesto(Integer codPresupuesto) {
        this.codPresupuesto = codPresupuesto;
    }

    public BeSolicitud getCodSolicitud() {
        return codSolicitud;
    }

    public void setCodSolicitud(BeSolicitud codSolicitud) {
        this.codSolicitud = codSolicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codDetallePresupuesto != null ? codDetallePresupuesto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PeDetallepresupuesto)) {
            return false;
        }
        PeDetallepresupuesto other = (PeDetallepresupuesto) object;
        if ((this.codDetallePresupuesto == null && other.codDetallePresupuesto != null) || (this.codDetallePresupuesto != null && !this.codDetallePresupuesto.equals(other.codDetallePresupuesto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.sipre.entidad.PeDetallepresupuesto[ codDetallePresupuesto=" + codDetallePresupuesto + " ]";
    }
    
}
