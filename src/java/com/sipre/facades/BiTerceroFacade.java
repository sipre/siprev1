/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.facades;

import com.sipre.entidad.BiTercero;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author efar
 */
@Stateless
public class BiTerceroFacade extends AbstractFacade<BiTercero> {

    @PersistenceContext(unitName = "siprePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BiTerceroFacade() {
        super(BiTercero.class);
    }
    
    public BiTercero login(String documento, String clave) {
        Integer doc = Integer.parseInt(documento);
         TypedQuery<BiTercero> query =
                getEntityManager().createQuery(
                "SELECT b FROM BiTercero b WHERE b.codTercero = :doc AND b.clave = :clv ", BiTercero.class);
        query.setParameter("doc", doc);
        query.setParameter("clv", clave);    
        
     try {
            return query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
    
}
