/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sipre.facades;

import com.sipre.entidad.GnPermiso;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author alejo
 */
@Stateless
public class GnPermisoFacade extends AbstractFacade<GnPermiso> {

    @PersistenceContext(unitName = "siprePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GnPermisoFacade() {
        super(GnPermiso.class);
    }
    
}
